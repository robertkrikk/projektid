import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.*;

import static java.util.stream.Collectors.toMap;

public class WordFrequencyTask {

    private static final int MAX_ITEMS = 10;
    private static final String FREQUENCIES = "TOP10 FREQUENCIES";
    Set<String> wordSet = new HashSet<String>();
    List<String> wordList = new ArrayList<String>();
    Map<String, Integer> wordFrequencies = new HashMap<String, Integer>();

    WordFrequencyTask() {
    }

    public List<String> readFile(String fileName) {
        List<String> fileStrings = new ArrayList<>();
        try {
            File file = new File(fileName);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String data = scanner.nextLine();
                String[] words = data.replaceAll("[^a-zA-Z ]", "").toLowerCase().split("\\s+");
                fileStrings.addAll(Arrays.asList(words));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return fileStrings;
    }

    public void getSubStrings(String word) {
        for (int i = 0; i < word.length(); i++) {
            for (int j = i + 1; j <= word.length(); j++) {
                String subString = word.substring(i, j);
                if (subString.length() > 3) {
                    wordList.add(subString);
                    wordSet.add(subString);
                }
            }
        }
    }

    public int getTotalWordCount(Map<String, Integer> map) {
        int counter = 0;
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            counter += entry.getValue();
        }
        return counter;
    }


    public void getWordFrequencies(String fileName) {
        List<String> fileStrings = readFile(fileName);
        for (String fileString : fileStrings) {
            getSubStrings(fileString);
        }
        for (String setString : wordSet) {
            wordFrequencies.put(setString, Collections.frequency(wordList, setString));
        }
        int totalWordCount = getTotalWordCount(wordFrequencies);
        int counter = 0;
        Map<String, Integer> sorted = wordFrequencies
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(
                        toMap(e -> e.getKey(), e -> e.getValue(), (e1, e2) -> e2,
                                LinkedHashMap::new));
        System.out.println(FREQUENCIES);
        for (Map.Entry<String, Integer> entry : sorted.entrySet()) {
            if (counter < MAX_ITEMS) {
                double percentage = (double) entry.getValue() / getTotalWordCount(sorted) * 100;
                DecimalFormat df = new DecimalFormat("#.##");
                percentage = Double.valueOf(df.format(percentage));
                System.out.println(entry.getKey() + " : " + percentage);
                counter++;
            }
        }
    }

    public static void main(String[] args) {
        WordFrequencyTask wordFrequencyTask = new WordFrequencyTask();
        wordFrequencyTask.getWordFrequencies("text");

    }


}
