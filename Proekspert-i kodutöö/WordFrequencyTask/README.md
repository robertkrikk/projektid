**Kodutöö lahenduse põhimõte**

Jagasin oma koduutöö lahendamise 3 ossa:

**1) Failist lugemine**

```java

public List<String> readFile(String fileName) {
        List<String> fileStrings = new ArrayList<>();
        try {
            File file = new File(fileName);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String data = scanner.nextLine();
                String[] words = data.replaceAll("[^a-zA-Z ]", "").toLowerCase().split("\\s+");
                fileStrings.addAll(Arrays.asList(words));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return fileStrings;
    }

```

Lõin funktsiooni, millele antakse kaasa tekstifaili nimi. Funktsioon tagastab sõnedest koosneva listi, kui on antud korrektne failinimi, vastasel juhul visatakse erind.
Sõned valitakse listi selle põhjal, kas tegu on sõnaga või mitte.
Sõnade otsimiseks kasutasin regexit, millega otsisin igast failireast sõnesid, mis algavad tähtedega ning mille lõpetab ükskõik mis muu karakter peale tähe.

**2) Sõnede töötlemine**

```java

public void getSubStrings(String word) {
        for (int i = 0; i < word.length(); i++) {
            for (int j = i + 1; j <= word.length(); j++) {
                String subString = word.substring(i, j);
                if (subString.length() > 3) {
                    wordList.add(subString);
                    wordSet.add(subString);
                }
            }
        }
    }
    
```

Lõin funktsiooni, mis otsiks igast failist leitud sõnest alamsõned. Kasutasin selleks lihtsaimat meetodid 2 tsükli näol, millega otsisin kõikidest 
alamsõnede kombinatsioonidest sõned, mille pikkus on üle 3 tähe. Leitud sõned lisasin klassimuutujatesse wordList, mille kaudu sain hiljem sõnede arvu, ja klassimuutujasse wordSet,
kus hoidsin vaid unikaalseid väärtusi, et hiljem oleks mugav sagedustabel koostada Map-i näol.

**3) Sagedustabeli koostamine**

```java
public void getWordFrequencies(String fileName) {
        List<String> fileStrings = readFile(fileName);
        for (String fileString : fileStrings) {
            getSubStrings(fileString);
        }
        for (String setString : wordSet) {
            wordFrequencies.put(setString, Collections.frequency(wordList, setString));
        }
        int totalWordCount = getTotalWordCount(wordFrequencies);
        int counter = 0;
        Map<String, Integer> sorted = wordFrequencies
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(
                        toMap(e -> e.getKey(), e -> e.getValue(), (e1, e2) -> e2,
                                LinkedHashMap::new));
        System.out.println(FREQUENCIES);
        for (Map.Entry<String, Integer> entry : sorted.entrySet()) {
            if (counter < MAX_ITEMS) {
                double percentage = (double) entry.getValue() / getTotalWordCount(sorted) * 100;
                DecimalFormat df = new DecimalFormat("#.##");
                percentage = Double.valueOf(df.format(percentage));
                System.out.println(entry.getKey() + " : " + percentage);
                counter++;
            }
        }
    }
```

Kasutasin eelnevalt loodud funktsioone, et luua sagedustabel konsooli. Sorteerisin Map-i ära kahanevalt streami kasutades ja lasin funktsioonil vaid 10 esimest väärtust konsooli kuvada. Lisaks sellele vormisin
sagedused 2 kohta peale koma.


**Testjuhtumid**

**1.**Lähtusin alguses "alla bella kella" näitest, ning kontrollisin, et tulem tuleks sama ka järgmiste juhtudega:

Sisendid:

1) ```"alla. bella kella"``` - tegu on siiski sõnaga, kui sõna lõpetab näiteks punkt.
2) ```"Alla bellA keLLa"``` - tegu on siiski sõnaga, kui sõna sisaldab suurt tähte.
3) ```"alla bella kella"``` - kui tühikute asemel on reavahetus või mitu tühikut, peab tulema samuti sama tulem.

Tulem:

```
TOP10 FREQUENCIES
ella : 28.57
kella : 14.29
kell : 14.29
bella : 14.29
alla : 14.29
bell : 14.29
```


**2.**Edasi kontrollisin juhtumit, kus peaks kuvatama üle 10 alamsõne. Siin kontrollisin kahte asja - protsendid peavad kokku tulema 100% või vähem, protsendid peavad korrektsed olema.

Sisend: ```"allabellakella"```

Tulem:

```
TOP10 FREQUENCIES
ella : 3.03
abellakell : 1.52
allabe : 1.52
allab : 1.52
allabel : 1.52
labellakell : 1.52
labellakella : 1.52
bellak : 1.52
llab : 1.52
labellakel : 1.52
```

Sarnaselt näitele on alamsõne ella sagedus 2x suurem teistest ning kuvatud ongi maksimaalselt 10 sõne

**3.** Tühi fail:

Sisend:

Tulem:

```
TOP10 FREQUENCIES
```
 **4.** Vigane failinimi:

Tulem:
```java
java.io.FileNotFoundException: text2 (The system cannot find the file specified)
	at java.base/java.io.FileInputStream.open0(Native Method)
	at java.base/java.io.FileInputStream.open(FileInputStream.java:219)
	at java.base/java.io.FileInputStream.<init>(FileInputStream.java:157)
	at java.base/java.util.Scanner.<init>(Scanner.java:639)
	at WordFrequencyTask.readFile(WordFrequencyTask.java:23)
	at WordFrequencyTask.getWordFrequencies(WordFrequencyTask.java:57)
	at WordFrequencyTask.main(WordFrequencyTask.java:87)
```


**Pastakaülesanne**

Pastaka testimisel lähtusin kolmest vaatepunktist:

1) Funktsionaalsus - OK

Pastaka põhifunktsioon kirjutamise näol on korras- tindijälg korrektne ning pastakapea tuleb ilusti välja nupu vajutuse peale.

2) Disain - OK

Pastaka disain on samuti defektideta ning näeb kaunilt kaasaegne välja.

3) Detailide seis - 50/50

Pastaka avamisel ning komponentide vaatlusel tundub kõik OK, kuid pastakapea nupu küljes olev kinnitusdetail loksub tugevalt.

Pastaka testimisest tundub mulle, et prototüübi põhjal on pastaka masstootmisesse panemine ohtlik - esines defekt pastakapea nupu küljes oleval detailil, millele tuleks lahendus leida enne.
