using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers
{
    [Authorize]
    public class TeamsInClubs : Controller
    {
        private readonly AppDbContext _context;

        public TeamsInClubs(AppDbContext context)
        {
            _context = context;
        }

        // GET: TeamsInClubs
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.TeamsInClub.Include(t => t.Club).Include(t => t.Team);
            return View(await appDbContext.ToListAsync());
        }

        // GET: TeamsInClubs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teamInClub = await _context.TeamsInClub
                .Include(t => t.Club)
                .Include(t => t.Team)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (teamInClub == null)
            {
                return NotFound();
            }

            return View(teamInClub);
        }

        // GET: TeamsInClubs/Create
        public IActionResult Create()
        {
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "ClubName");
            ViewData["TeamId"] = new SelectList(_context.Teams, "Id", "TeamName");
            return View();
        }

        // POST: TeamsInClubs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TeamId,ClubId,Id")] TeamInClub teamInClub)
        {
            if (ModelState.IsValid)
            {
                _context.Add(teamInClub);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "ClubName", teamInClub.ClubId);
            ViewData["TeamId"] = new SelectList(_context.Teams, "Id", "TeamName", teamInClub.TeamId);
            return View(teamInClub);
        }

        // GET: TeamsInClubs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teamInClub = await _context.TeamsInClub.FindAsync(id);
            if (teamInClub == null)
            {
                return NotFound();
            }
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "ClubName", teamInClub.ClubId);
            ViewData["TeamId"] = new SelectList(_context.Teams, "Id", "TeamName", teamInClub.TeamId);
            return View(teamInClub);
        }

        // POST: TeamsInClubs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("TeamId,ClubId,Id")] TeamInClub teamInClub)
        {
            if (id != teamInClub.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(teamInClub);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TeamInClubExists(teamInClub.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "ClubName", teamInClub.ClubId);
            ViewData["TeamId"] = new SelectList(_context.Teams, "Id", "TeamName", teamInClub.TeamId);
            return View(teamInClub);
        }

        // GET: TeamsInClubs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teamInClub = await _context.TeamsInClub
                .Include(t => t.Club)
                .Include(t => t.Team)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (teamInClub == null)
            {
                return NotFound();
            }

            return View(teamInClub);
        }

        // POST: TeamsInClubs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var teamInClub = await _context.TeamsInClub.FindAsync(id);
            _context.TeamsInClub.Remove(teamInClub);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TeamInClubExists(int id)
        {
            return _context.TeamsInClub.Any(e => e.Id == id);
        }
    }
}
