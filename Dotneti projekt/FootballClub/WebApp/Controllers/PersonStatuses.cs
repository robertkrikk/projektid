using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers
{
    [Authorize]
    public class PersonStatuses : Controller
    {
        private readonly AppDbContext _context;

        public PersonStatuses(AppDbContext context)
        {
            _context = context;
        }

        // GET: PersonStatuses
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.PersonStatuses.Include(p => p.Person).Include(p => p.Status);
            return View(await appDbContext.ToListAsync());
        }

        // GET: PersonStatuses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var personStatus = await _context.PersonStatuses
                .Include(p => p.Person)
                .Include(p => p.Status)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (personStatus == null)
            {
                return NotFound();
            }

            return View(personStatus);
        }

        // GET: PersonStatuses/Create
        public IActionResult Create()
        {
            ViewData["PersonId"] = new SelectList(_context.Persons, "Id", "FirstName");
            ViewData["StatusId"] = new SelectList(_context.Statuses, "Id", "StatusName");
            return View();
        }

        // POST: PersonStatuses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PersonId,StatusId,Comment,Id")] PersonStatus personStatus)
        {
            if (ModelState.IsValid)
            {
                _context.Add(personStatus);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["PersonId"] = new SelectList(_context.Persons, "Id", "FirstName", personStatus.PersonId);
            ViewData["StatusId"] = new SelectList(_context.Statuses, "Id", "StatusName", personStatus.StatusId);
            return View(personStatus);
        }

        // GET: PersonStatuses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var personStatus = await _context.PersonStatuses.FindAsync(id);
            if (personStatus == null)
            {
                return NotFound();
            }
            ViewData["PersonId"] = new SelectList(_context.Persons, "Id", "FirstName", personStatus.PersonId);
            ViewData["StatusId"] = new SelectList(_context.Statuses, "Id", "StatusName", personStatus.StatusId);
            return View(personStatus);
        }

        // POST: PersonStatuses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PersonId,StatusId,Comment,Id")] PersonStatus personStatus)
        {
            if (id != personStatus.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(personStatus);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PersonStatusExists(personStatus.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PersonId"] = new SelectList(_context.Persons, "Id", "FirstName", personStatus.PersonId);
            ViewData["StatusId"] = new SelectList(_context.Statuses, "Id", "StatusName", personStatus.StatusId);
            return View(personStatus);
        }

        // GET: PersonStatuses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var personStatus = await _context.PersonStatuses
                .Include(p => p.Person)
                .Include(p => p.Status)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (personStatus == null)
            {
                return NotFound();
            }

            return View(personStatus);
        }

        // POST: PersonStatuses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var personStatus = await _context.PersonStatuses.FindAsync(id);
            _context.PersonStatuses.Remove(personStatus);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PersonStatusExists(int id)
        {
            return _context.PersonStatuses.Any(e => e.Id == id);
        }
    }
}
