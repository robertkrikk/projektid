using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace WebApp.Controllers
{
    public class ClubSponsorsController : Controller
    {
        private readonly AppDbContext _context;

        public ClubSponsorsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: ClubSponsors
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.ClubSponsors.Include(c => c.Club).Include(c => c.Sponsor);
            return View(await appDbContext.ToListAsync());
        }

        // GET: ClubSponsors/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var clubSponsors = await _context.ClubSponsors
                .Include(c => c.Club)
                .Include(c => c.Sponsor)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (clubSponsors == null)
            {
                return NotFound();
            }

            return View(clubSponsors);
        }

        // GET: ClubSponsors/Create
        public IActionResult Create()
        {
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "ClubName");
            ViewData["SponsorId"] = new SelectList(_context.Sponsors, "Id", "Id");
            return View();
        }

        // POST: ClubSponsors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SponsorId,ClubId,Comment,SponshorshipSize,Id")] ClubSponsors clubSponsors)
        {
            if (ModelState.IsValid)
            {
                _context.Add(clubSponsors);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "ClubName", clubSponsors.ClubId);
            ViewData["SponsorId"] = new SelectList(_context.Sponsors, "Id", "Id", clubSponsors.SponsorId);
            return View(clubSponsors);
        }

        // GET: ClubSponsors/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var clubSponsors = await _context.ClubSponsors.FindAsync(id);
            if (clubSponsors == null)
            {
                return NotFound();
            }
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "ClubName", clubSponsors.ClubId);
            ViewData["SponsorId"] = new SelectList(_context.Sponsors, "Id", "Id", clubSponsors.SponsorId);
            return View(clubSponsors);
        }

        // POST: ClubSponsors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SponsorId,ClubId,Comment,SponshorshipSize,Id")] ClubSponsors clubSponsors)
        {
            if (id != clubSponsors.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(clubSponsors);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClubSponsorsExists(clubSponsors.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "ClubName", clubSponsors.ClubId);
            ViewData["SponsorId"] = new SelectList(_context.Sponsors, "Id", "Id", clubSponsors.SponsorId);
            return View(clubSponsors);
        }

        // GET: ClubSponsors/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var clubSponsors = await _context.ClubSponsors
                .Include(c => c.Club)
                .Include(c => c.Sponsor)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (clubSponsors == null)
            {
                return NotFound();
            }

            return View(clubSponsors);
        }

        // POST: ClubSponsors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var clubSponsors = await _context.ClubSponsors.FindAsync(id);
            _context.ClubSponsors.Remove(clubSponsors);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ClubSponsorsExists(int id)
        {
            return _context.ClubSponsors.Any(e => e.Id == id);
        }
    }
}
