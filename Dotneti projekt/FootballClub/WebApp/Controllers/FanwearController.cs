using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace WebApp.Controllers
{
    public class FanwearController : Controller
    {
        private readonly AppDbContext _context;

        public FanwearController(AppDbContext context)
        {
            _context = context;
        }

        // GET: Fanwear
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.Fanwear.Include(f => f.Club);
            return View(await appDbContext.ToListAsync());
        }

        // GET: Fanwear/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fanwear = await _context.Fanwear
                .Include(f => f.Club)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (fanwear == null)
            {
                return NotFound();
            }

            return View(fanwear);
        }

        // GET: Fanwear/Create
        public IActionResult Create()
        {
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "ClubName");
            return View();
        }

        // POST: Fanwear/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FanwearName,FanwearSize,FanwearPrice,Description,ClubId,Id")] Fanwear fanwear)
        {
            if (ModelState.IsValid)
            {
                _context.Add(fanwear);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "ClubName", fanwear.ClubId);
            return View(fanwear);
        }

        // GET: Fanwear/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fanwear = await _context.Fanwear.FindAsync(id);
            if (fanwear == null)
            {
                return NotFound();
            }
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "ClubName", fanwear.ClubId);
            return View(fanwear);
        }

        // POST: Fanwear/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FanwearName,FanwearSize,FanwearPrice,Description,ClubId,Id")] Fanwear fanwear)
        {
            if (id != fanwear.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(fanwear);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FanwearExists(fanwear.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "ClubName", fanwear.ClubId);
            return View(fanwear);
        }

        // GET: Fanwear/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var fanwear = await _context.Fanwear
                .Include(f => f.Club)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (fanwear == null)
            {
                return NotFound();
            }

            return View(fanwear);
        }

        // POST: Fanwear/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var fanwear = await _context.Fanwear.FindAsync(id);
            _context.Fanwear.Remove(fanwear);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FanwearExists(int id)
        {
            return _context.Fanwear.Any(e => e.Id == id);
        }
    }
}
