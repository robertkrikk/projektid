using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace WebApp.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonStatuses : ControllerBase
    {
        private readonly AppDbContext _context;

        public PersonStatuses(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/PersonStatuses
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PersonStatus>>> GetPersonStatuses()
        {
            return await _context.PersonStatuses.ToListAsync();
        }

        // GET: api/PersonStatuses/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PersonStatus>> GetPersonStatus(int id)
        {
            var personStatus = await _context.PersonStatuses.FindAsync(id);

            if (personStatus == null)
            {
                return NotFound();
            }

            return personStatus;
        }

        // PUT: api/PersonStatuses/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPersonStatus(int id, PersonStatus personStatus)
        {
            if (id != personStatus.Id)
            {
                return BadRequest();
            }

            _context.Entry(personStatus).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PersonStatusExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PersonStatuses
        [HttpPost]
        public async Task<ActionResult<PersonStatus>> PostPersonStatus(PersonStatus personStatus)
        {
            _context.PersonStatuses.Add(personStatus);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPersonStatus", new { id = personStatus.Id }, personStatus);
        }

        // DELETE: api/PersonStatuses/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<PersonStatus>> DeletePersonStatus(int id)
        {
            var personStatus = await _context.PersonStatuses.FindAsync(id);
            if (personStatus == null)
            {
                return NotFound();
            }

            _context.PersonStatuses.Remove(personStatus);
            await _context.SaveChangesAsync();

            return personStatus;
        }

        private bool PersonStatusExists(int id)
        {
            return _context.PersonStatuses.Any(e => e.Id == id);
        }
    }
}
