using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace WebApp.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClubSponsorsController : ControllerBase
    {
        private readonly AppDbContext _context;

        public ClubSponsorsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/ClubSponsorsController-actions
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ClubSponsors>>> GetClubSponsors()
        {
            return await _context.ClubSponsors.ToListAsync();
        }

        // GET: api/ClubSponsorsController-actions/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ClubSponsors>> GetClubSponsors(int id)
        {
            var clubSponsors = await _context.ClubSponsors.FindAsync(id);

            if (clubSponsors == null)
            {
                return NotFound();
            }

            return clubSponsors;
        }

        // PUT: api/ClubSponsorsController-actions/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutClubSponsors(int id, ClubSponsors clubSponsors)
        {
            if (id != clubSponsors.Id)
            {
                return BadRequest();
            }

            _context.Entry(clubSponsors).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClubSponsorsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ClubSponsorsController-actions
        [HttpPost]
        public async Task<ActionResult<ClubSponsors>> PostClubSponsors(ClubSponsors clubSponsors)
        {
            _context.ClubSponsors.Add(clubSponsors);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetClubSponsors", new { id = clubSponsors.Id }, clubSponsors);
        }

        // DELETE: api/ClubSponsorsController-actions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ClubSponsors>> DeleteClubSponsors(int id)
        {
            var clubSponsors = await _context.ClubSponsors.FindAsync(id);
            if (clubSponsors == null)
            {
                return NotFound();
            }

            _context.ClubSponsors.Remove(clubSponsors);
            await _context.SaveChangesAsync();

            return clubSponsors;
        }

        private bool ClubSponsorsExists(int id)
        {
            return _context.ClubSponsors.Any(e => e.Id == id);
        }
    }
}
