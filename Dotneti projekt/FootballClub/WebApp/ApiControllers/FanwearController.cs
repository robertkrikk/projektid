using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace WebApp.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FanwearController : ControllerBase
    {
        private readonly AppDbContext _context;

        public FanwearController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Fanwear
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Fanwear>>> GetFanwear()
        {
            return await _context.Fanwear.ToListAsync();
        }

        // GET: api/Fanwear/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Fanwear>> GetFanwear(int id)
        {
            var fanwear = await _context.Fanwear.FindAsync(id);

            if (fanwear == null)
            {
                return NotFound();
            }

            return fanwear;
        }

        // PUT: api/Fanwear/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFanwear(int id, Fanwear fanwear)
        {
            if (id != fanwear.Id)
            {
                return BadRequest();
            }

            _context.Entry(fanwear).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FanwearExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Fanwear
        [HttpPost]
        public async Task<ActionResult<Fanwear>> PostFanwear(Fanwear fanwear)
        {
            _context.Fanwear.Add(fanwear);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFanwear", new { id = fanwear.Id }, fanwear);
        }

        // DELETE: api/Fanwear/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Fanwear>> DeleteFanwear(int id)
        {
            var fanwear = await _context.Fanwear.FindAsync(id);
            if (fanwear == null)
            {
                return NotFound();
            }

            _context.Fanwear.Remove(fanwear);
            await _context.SaveChangesAsync();

            return fanwear;
        }

        private bool FanwearExists(int id)
        {
            return _context.Fanwear.Any(e => e.Id == id);
        }
    }
}
