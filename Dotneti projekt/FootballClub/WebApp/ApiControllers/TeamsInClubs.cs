using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace WebApp.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsInClubs : ControllerBase
    {
        private readonly AppDbContext _context;

        public TeamsInClubs(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/TeamsInClubs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamInClub>>> GetTeamsInClub()
        {
            return await _context.TeamsInClub.ToListAsync();
        }

        // GET: api/TeamsInClubs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TeamInClub>> GetTeamInClub(int id)
        {
            var teamInClub = await _context.TeamsInClub.FindAsync(id);

            if (teamInClub == null)
            {
                return NotFound();
            }

            return teamInClub;
        }

        // PUT: api/TeamsInClubs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTeamInClub(int id, TeamInClub teamInClub)
        {
            if (id != teamInClub.Id)
            {
                return BadRequest();
            }

            _context.Entry(teamInClub).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TeamInClubExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TeamsInClubs
        [HttpPost]
        public async Task<ActionResult<TeamInClub>> PostTeamInClub(TeamInClub teamInClub)
        {
            _context.TeamsInClub.Add(teamInClub);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTeamInClub", new { id = teamInClub.Id }, teamInClub);
        }

        // DELETE: api/TeamsInClubs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TeamInClub>> DeleteTeamInClub(int id)
        {
            var teamInClub = await _context.TeamsInClub.FindAsync(id);
            if (teamInClub == null)
            {
                return NotFound();
            }

            _context.TeamsInClub.Remove(teamInClub);
            await _context.SaveChangesAsync();

            return teamInClub;
        }

        private bool TeamInClubExists(int id)
        {
            return _context.TeamsInClub.Any(e => e.Id == id);
        }
    }
}
