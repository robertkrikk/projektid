using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL;
using Domain;

namespace WebApp.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonsInClubs : ControllerBase
    {
        private readonly AppDbContext _context;

        public PersonsInClubs(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/PersonsInClubs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PersonInClub>>> GetPersonsInClubs()
        {
            return await _context.PersonsInClubs.ToListAsync();
        }

        // GET: api/PersonsInClubs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PersonInClub>> GetPersonInClub(int id)
        {
            var personInClub = await _context.PersonsInClubs.FindAsync(id);

            if (personInClub == null)
            {
                return NotFound();
            }

            return personInClub;
        }

        // PUT: api/PersonsInClubs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPersonInClub(int id, PersonInClub personInClub)
        {
            if (id != personInClub.Id)
            {
                return BadRequest();
            }

            _context.Entry(personInClub).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PersonInClubExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PersonsInClubs
        [HttpPost]
        public async Task<ActionResult<PersonInClub>> PostPersonInClub(PersonInClub personInClub)
        {
            _context.PersonsInClubs.Add(personInClub);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPersonInClub", new { id = personInClub.Id }, personInClub);
        }

        // DELETE: api/PersonsInClubs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<PersonInClub>> DeletePersonInClub(int id)
        {
            var personInClub = await _context.PersonsInClubs.FindAsync(id);
            if (personInClub == null)
            {
                return NotFound();
            }

            _context.PersonsInClubs.Remove(personInClub);
            await _context.SaveChangesAsync();

            return personInClub;
        }

        private bool PersonInClubExists(int id)
        {
            return _context.PersonsInClubs.Any(e => e.Id == id);
        }
    }
}
