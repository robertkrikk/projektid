using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.Base.EF.Repositories;
using Domain;

namespace DAL.Repositories
{
    public class TeamInClubRepository: BaseRepository<TeamInClub>, ITeamInClubRepository
    {
        public TeamInClubRepository(IDataContext dataContext) : base(dataContext)
        {
            
        }
    }
}