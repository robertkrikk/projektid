using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.Base.EF.Repositories;
using Domain;

namespace DAL.Repositories
{
    public class FanwearRepository : BaseRepository<Fanwear>, IFanwearRepository
    {
        public FanwearRepository(IDataContext dataContext) : base(dataContext)
        {
            
        }
    }
}