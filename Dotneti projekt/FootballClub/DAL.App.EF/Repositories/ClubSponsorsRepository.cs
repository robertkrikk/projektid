using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.Base.EF.Repositories;
using Domain;

namespace DAL.Repositories
{
    public class ClubSponsorsRepository : BaseRepository<ClubSponsors>, IClubSponsorsRepository
    {
        public ClubSponsorsRepository(IDataContext dataContext) : base(dataContext)
        {
            
        }
    }
}