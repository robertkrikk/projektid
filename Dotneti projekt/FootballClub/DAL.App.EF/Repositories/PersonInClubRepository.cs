using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.Base.EF.Repositories;
using Domain;

namespace DAL.Repositories
{
    public class PersonInClubRepository : BaseRepository<PersonInClub>, IPersonInClubRepository
    {
        public PersonInClubRepository(IDataContext dataContext) : base(dataContext)
        {
            
        }
    }
}