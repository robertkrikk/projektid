using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.Base.EF.Repositories;
using Domain;

namespace DAL.Repositories
{
    public class PersonStatusRepository : BaseRepository<PersonStatus>, IPersonStatusRepository
    {
        public PersonStatusRepository(IDataContext dataContext) : base(dataContext)
        {
            
        }
    }
}