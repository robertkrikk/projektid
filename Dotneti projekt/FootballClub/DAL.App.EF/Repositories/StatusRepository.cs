using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.Base.EF.Repositories;
using Domain;

namespace DAL.Repositories
{
    public class StatusRepository: BaseRepository<Status>, IStatusRepository
    {
        public StatusRepository(IDataContext dataContext) : base(dataContext)
        {
            
        }
    }
}