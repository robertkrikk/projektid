using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.Base.EF.Repositories;
using Domain;

namespace DAL.Repositories
{
    public class GameRepository: BaseRepository<Game>, IGameRepository
    {

        public GameRepository(IDataContext dataContext) : base(dataContext)
        {
            
        }
    }
}