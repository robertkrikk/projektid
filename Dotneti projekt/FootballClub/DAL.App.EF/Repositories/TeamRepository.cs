using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.Base.EF.Repositories;
using Domain;

namespace DAL.Repositories
{
    public class TeamRepository: BaseRepository<Team>, ITeamRepository
    {
        public TeamRepository(IDataContext dataContext) : base(dataContext)
        {
            
        }
    }
}