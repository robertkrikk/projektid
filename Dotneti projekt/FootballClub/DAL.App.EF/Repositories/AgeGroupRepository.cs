using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.Base.EF.Repositories;
using Domain;

namespace DAL.Repositories
{
    public class AgeGroupRepository: BaseRepository<AgeGroup>, IAgeGroupRepository
    {
        public AgeGroupRepository(IDataContext dataContext) : base(dataContext)
        {
            
        }
    }
}