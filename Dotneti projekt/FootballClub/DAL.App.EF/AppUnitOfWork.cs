using System;
using System.Threading.Tasks;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using Contracts.DAL.Base.Helper;
using Contracts.DAL.Base.Repositories;

namespace DAL
{
    public class AppUnitOfWork : IAppUnitOfWork
    {
        private readonly AppDbContext _appDbContext;

        private readonly IRepositoryProvider _repositoryProvider;

        public IPersonRepository Persons =>
            _repositoryProvider.GetRepository<IPersonRepository>();

        public IAgeGroupRepository AgeGroups =>
            _repositoryProvider.GetRepository<IAgeGroupRepository>();
        
        public IClubSponsorsRepository ClubSponsors =>
            _repositoryProvider.GetRepository<IClubSponsorsRepository>();
        
        public IFacilityRepository Facilities =>
            _repositoryProvider.GetRepository<IFacilityRepository>();
        
        public IFanwearRepository Fanwear =>
            _repositoryProvider.GetRepository<IFanwearRepository>();
        
        public IGameRepository Games =>
            _repositoryProvider.GetRepository<IGameRepository>();
        
        public IPersonInClubRepository PersonsInClub =>
            _repositoryProvider.GetRepository<IPersonInClubRepository>();
        
        public IPersonStatusRepository PersonStatuses =>
            _repositoryProvider.GetRepository<IPersonStatusRepository>();
        
        public IRoleRepository Roles =>
            _repositoryProvider.GetRepository<IRoleRepository>();
        
        public ISponsorRepository Sponsors =>
            _repositoryProvider.GetRepository<ISponsorRepository>();
        
        public IStatusRepository Statuses =>
            _repositoryProvider.GetRepository<IStatusRepository>();
        
        public ITeamInClubRepository TeamsInClub =>
            _repositoryProvider.GetRepository<ITeamInClubRepository>();

        public ITeamRepository Teams =>
            _repositoryProvider.GetRepository<ITeamRepository>();
        
        public ITournamentRepository Tournaments =>
            _repositoryProvider.GetRepository<ITournamentRepository>();
        
        public ITrainingRepository Trainings =>
            _repositoryProvider.GetRepository<ITrainingRepository>();
        
        public IClubRepository Clubs =>
            _repositoryProvider.GetRepository<IClubRepository>();
        
        
        public IBaseRepositoryAsync<TEntity> BaseRepository<TEntity>() where TEntity : class, IBaseEntity, new() =>
            _repositoryProvider.GetRepositoryForEntity<TEntity>();


        public AppUnitOfWork(IDataContext dataContext, IRepositoryProvider repositoryProvider)
        {
            _appDbContext = (dataContext as AppDbContext) ?? throw new ArgumentNullException(nameof(dataContext));
            _repositoryProvider = repositoryProvider;
        }




        public virtual int SaveChanges()
        {
            return _appDbContext.SaveChanges();
        }

        public virtual async Task<int> SaveChangesAsync()
        {
            return await _appDbContext.SaveChangesAsync();
        }
    
        
    }
}