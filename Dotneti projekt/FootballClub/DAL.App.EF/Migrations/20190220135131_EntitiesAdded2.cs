﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class EntitiesAdded2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Fanwear",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    FanwearName = table.Column<string>(maxLength: 64, nullable: false),
                    FanwearSize = table.Column<string>(maxLength: 2, nullable: false),
                    FanwearPrice = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 64, nullable: false),
                    ClubId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fanwear", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Fanwear_Clubs_ClubId",
                        column: x => x.ClubId,
                        principalTable: "Clubs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Sponsors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    SponsorName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sponsors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ClubSponsors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    SponsorId = table.Column<int>(nullable: false),
                    ClubId = table.Column<int>(nullable: false),
                    Comment = table.Column<string>(nullable: true),
                    SponshorshipSize = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClubSponsors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClubSponsors_Clubs_ClubId",
                        column: x => x.ClubId,
                        principalTable: "Clubs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ClubSponsors_Sponsors_SponsorId",
                        column: x => x.SponsorId,
                        principalTable: "Sponsors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClubSponsors_ClubId",
                table: "ClubSponsors",
                column: "ClubId");

            migrationBuilder.CreateIndex(
                name: "IX_ClubSponsors_SponsorId",
                table: "ClubSponsors",
                column: "SponsorId");

            migrationBuilder.CreateIndex(
                name: "IX_Fanwear_ClubId",
                table: "Fanwear",
                column: "ClubId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClubSponsors");

            migrationBuilder.DropTable(
                name: "Fanwear");

            migrationBuilder.DropTable(
                name: "Sponsors");
        }
    }
}
