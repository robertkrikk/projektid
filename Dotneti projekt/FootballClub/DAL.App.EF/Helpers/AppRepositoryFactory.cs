using Contracts.DAL.App.Repositories;
using DAL.Base.EF.Helpers;
using DAL.Repositories;
using Domain;

namespace DAL.Helpers
{
    public class AppRepositoryFactory : BaseRepositoryFactory
    {

        public AppRepositoryFactory()
        {
            RepositoryCreationMethods.Add(typeof(IPersonRepository), 
                dataContext => new PersonRepository(dataContext));
            
            RepositoryCreationMethods.Add(typeof(IAgeGroupRepository), 
                dataContext => new AgeGroupRepository(dataContext));
            
            RepositoryCreationMethods.Add(typeof(IClubRepository), 
                dataContext => new ClubRepository(dataContext));
            
            RepositoryCreationMethods.Add(typeof(IFacilityRepository), 
                dataContext => new FacilityRepository(dataContext));
            
            RepositoryCreationMethods.Add(typeof(IFanwearRepository), 
                dataContext => new FanwearRepository(dataContext));
            
            RepositoryCreationMethods.Add(typeof(IGameRepository), 
                dataContext => new GameRepository(dataContext));
            
            RepositoryCreationMethods.Add(typeof(IPersonInClubRepository), 
                dataContext => new PersonInClubRepository(dataContext));
            
            RepositoryCreationMethods.Add(typeof(IPersonStatusRepository), 
                dataContext => new PersonStatusRepository(dataContext));
            
            RepositoryCreationMethods.Add(typeof(IRoleRepository), 
                dataContext => new RoleRepository(dataContext));
            
            RepositoryCreationMethods.Add(typeof(ISponsorRepository), 
                dataContext => new SponsorRepository(dataContext));
            
            RepositoryCreationMethods.Add(typeof(IStatusRepository), 
                dataContext => new StatusRepository(dataContext));
            
            RepositoryCreationMethods.Add(typeof(ITeamInClubRepository), 
                dataContext => new TeamInClubRepository(dataContext));
            
            RepositoryCreationMethods.Add(typeof(ITeamRepository), 
                dataContext => new TeamRepository(dataContext));
            
            RepositoryCreationMethods.Add(typeof(ITournamentRepository), 
                dataContext => new TournamentRepository(dataContext));
            
            
        }
        
    }
}