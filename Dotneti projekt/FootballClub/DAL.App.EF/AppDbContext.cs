﻿using System;
using System.Linq;
using Contracts.DAL.Base;
using Domain;
using Domain.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class AppDbContext : IdentityDbContext<AppUser, AppRole, int>, IDataContext
    {
        
        public DbSet<Club> Clubs { get; set; }
        public DbSet<Facility> Facilities { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Tournament> Tournaments { get; set; }
        public DbSet<Training> Trainings { get; set; }
        public DbSet<AgeGroup> AgeGroups { get; set; }
        public DbSet<PersonInClub> PersonsInClubs { get; set; }
        public DbSet<PersonStatus> PersonStatuses { get; set; }
        public DbSet<TeamInClub> TeamsInClub { get; set; }
        public DbSet<Sponsor> Sponsors { get; set; }
        public DbSet<ClubSponsors> ClubSponsors { get; set; }
        public DbSet<Fanwear> Fanwear { get; set; }
        
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // disable cascade delete
            foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }
        
    }
}