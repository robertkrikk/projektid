using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class AgeGroup : BaseEntity
    {
        
        [Required]
        public int LowerAgeLimit { get; set; }
        
        public int UpperAgeLimit { get; set; }
        
        [Required]
        public string AgeGroupName { get; set; }
        
    }
}