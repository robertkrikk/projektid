using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Team : BaseEntity
    {
        [MaxLength(64)]
        [MinLength(1)]
        [Required]
        public string TeamName { get; set; }
        
        public int AgeGroupId { get; set; }
        public AgeGroup AgeGroup { get; set; }
        
    }
}