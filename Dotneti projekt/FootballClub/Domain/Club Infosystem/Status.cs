using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Status : BaseEntity
    {
        [MaxLength(64)]
        [MinLength(1)]
        [Required]
        public string StatusType { get; set; }
        
        [MaxLength(64)]
        [MinLength(1)]
        [Required]
        public string StatusName { get; set; }
    }
}