using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Game : BaseEntity
    {
        
        public int TournamentId { get; set; }
        public Tournament Tournament { get; set; }
        
        public int FacilityId { get; set; }
        public Facility Facility { get; set; }
        
        [MaxLength(64)]
        [MinLength(1)]
        [Required]
        public string Result { get; set; }
        
        [MinLength(1)]
        public string Comment { get; set; }
        
    }
}