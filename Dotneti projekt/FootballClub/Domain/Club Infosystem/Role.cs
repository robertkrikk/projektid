using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Role : BaseEntity
    {
        
        [MaxLength(64)]
        [MinLength(1)]
        [Required]
        public string RoleName { get; set; }
    }
}