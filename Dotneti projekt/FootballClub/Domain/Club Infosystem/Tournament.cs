using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Tournament : BaseEntity
    {
        
        [MaxLength(64)]
        [MinLength(1)]
        [Required]
        public string TournamentName { get; set; }
        
        
        [MaxLength(64)]
        [MinLength(1)]
        [Required]
        public string TournamentType { get; set; }
        
        public int TeamId { get; set; }
        public Team Team { get; set; }
        
        public int AgeGroupId { get; set; }
        public AgeGroup AgeGroup { get; set; }
        
        
    }
}