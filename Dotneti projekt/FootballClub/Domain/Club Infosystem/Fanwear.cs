using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Fanwear : BaseEntity
    {
        [MaxLength(64)]
        [MinLength(1)]
        [Required]
        public string FanwearName { get; set; }
        
        [MaxLength(2)]
        [MinLength(1)]
        [Required]
        public string FanwearSize { get; set; }
        
        [Required]
        public int FanwearPrice { get; set; }
        
        [MaxLength(64)]
        [MinLength(1)]
        [Required]
        public string Description { get; set; }
        
        public int ClubId { get; set; }
        public Club Club { get; set; }
        
        
    }
}