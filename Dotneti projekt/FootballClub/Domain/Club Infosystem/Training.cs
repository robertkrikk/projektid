using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Training : BaseEntity
    {
        [MaxLength(64)]
        [MinLength(1)]
        [Required]
        public string TrainingType { get; set; }
        
        [Required]
        [MinLength(1)]
        public static string StartingDate { get; set; }
        public static DateTime StartingDateValue = DateTime.Parse(StartingDate);
        public static string StartingDateForMySql = StartingDateValue.ToString("yyyy-MM-dd HH:mm");
        
        [Required]
        public int Duration { get; set; }
        
        public int ParentTrainingId { get; set; }
        public Training ParentTraining { get; set; }
        
        public int FacilityId { get; set; }
        public Facility Facility { get; set; }
        
    }
}