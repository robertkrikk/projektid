using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class ClubSponsors : BaseEntity
    {
        public int SponsorId { get; set; }
        public Sponsor Sponsor { get; set; }
        
        public int ClubId { get; set; }
        public Club Club { get; set; }
        
        [Required]
        public static string StartingDate { get; set; }
        public static DateTime StartingDateValue = DateTime.Parse(StartingDate);
        public static string StartingDateForMySql = StartingDateValue.ToString("yyyy-MM-dd HH:mm");
        
        public static string EndingDate { get; set; }
        public static DateTime EndingDateValue = DateTime.Parse(EndingDate);
        public static string EndingDateForMySql = EndingDateValue.ToString("yyyy-MM-dd HH:mm");
        
        public string Comment { get; set; }
        
        public int SponshorshipSize { get; set; }
        
        
    }
}