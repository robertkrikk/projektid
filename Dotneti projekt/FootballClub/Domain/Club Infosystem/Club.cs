﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Club : BaseEntity
    {
        [MaxLength(64)]
        [MinLength(1)]
        [Required]
        public string ClubName { get; set; }
        
        public int ParentClubId { get; set; }
        public Club ParentClub { get; set; }
        
    }
}