using System;

namespace Domain
{
    public class News : BaseEntity
    {
        
        public String Content { get; set; }
        
        public static string PublishingDate { get; set; }
        public static DateTime PublishingDateValue = DateTime.Parse(PublishingDate);
        public static string StartingDateForMySql = PublishingDateValue.ToString("yyyy-MM-dd HH:mm");
    
        
        public int ClubId { get; set; }
        public Club Club { get; set; }
        
    }
}