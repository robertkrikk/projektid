using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Facility : BaseEntity
    {
        [MaxLength(64)]
        [MinLength(1)]
        [Required]
        public string FacilityName { get; set; }
        
        [MaxLength(64)]
        [MinLength(1)]
        [Required]
        public string Address { get; set; }
        
        
    }
}