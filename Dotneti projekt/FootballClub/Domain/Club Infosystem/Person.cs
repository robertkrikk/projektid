using System;
using System.ComponentModel.DataAnnotations;
using Domain.Identity;

namespace Domain
{
    public class Person : BaseEntity
    {
        [MaxLength(64)]
        [MinLength(1)]
        [Required]
        public string FirstName { get; set; }

        [MaxLength(64)]
        [MinLength(1)]
        [Required]
        public string LastName { get; set; }
        
        [Required]
        public string IdentificationCode { get; set; }
        
        [MaxLength(64)]
        [MinLength(1)]
        [Required]
        public string Nationality { get; set; }
        
        public int AppUserId { get; set; }
        public AppUser AppUser { get; set; }
        
        
        public string FirstLastName => FirstName + " " + LastName;
    }
}