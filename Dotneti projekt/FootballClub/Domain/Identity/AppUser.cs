using System.Collections.Generic;
using Contracts.DAL.Base;
using Microsoft.AspNetCore.Identity;

namespace Domain.Identity
{
    public class AppUser : IdentityUser<int>, IBaseEntity
    {
        
        public ICollection<Person> Persons { get; set; }
        
    }
}