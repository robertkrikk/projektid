using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;

namespace Contracts.DAL.App
{
    public interface IAppUnitOfWork : IUnitOfWork
    {
        IPersonRepository Persons { get;  }
        IAgeGroupRepository AgeGroups { get;  }
        IClubRepository Clubs { get; }
        IClubSponsorsRepository ClubSponsors { get; }
        IFacilityRepository Facilities { get; }
        IFanwearRepository Fanwear { get; }
        IGameRepository Games { get;  }
        IPersonInClubRepository PersonsInClub { get; }
        IPersonStatusRepository PersonStatuses { get; }
        IRoleRepository Roles { get; }
        ISponsorRepository Sponsors { get; }
        IStatusRepository Statuses { get;  }
        ITeamInClubRepository TeamsInClub { get; }
        ITeamRepository Teams { get; }
        ITournamentRepository Tournaments { get; }
        ITrainingRepository Trainings { get; }
        
    }
}