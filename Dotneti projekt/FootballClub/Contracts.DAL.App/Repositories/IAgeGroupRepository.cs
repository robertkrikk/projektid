using Contracts.DAL.Base.Repositories;
using Domain;

namespace Contracts.DAL.App.Repositories
{
    public interface IAgeGroupRepository : IBaseRepositoryAsync<AgeGroup>
    {
        
    }
}