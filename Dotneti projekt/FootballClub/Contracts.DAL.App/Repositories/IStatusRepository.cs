using Contracts.DAL.Base.Repositories;
using Domain;

namespace Contracts.DAL.App.Repositories
{
    public interface IStatusRepository : IBaseRepositoryAsync<Status>
    {
        
    }
}