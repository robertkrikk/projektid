package com.example.a15puzzle;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedList;

import static com.example.a15puzzle.Constants.NEW_GAME;
import static com.example.a15puzzle.Constants.REFRESH_RATE;
import static com.example.a15puzzle.Constants.SHUFFLE_1000_Times;
import static com.example.a15puzzle.Constants.TIMER_START;
import static com.example.a15puzzle.Constants.TIMER_STOP;
import static com.example.a15puzzle.Constants.TIMER_UPDATE;
import static com.example.a15puzzle.GameLogic.checkGameFinish;
import static com.example.a15puzzle.GameLogic.raiseMovesCounter;
import static com.example.a15puzzle.GameLogic.shuffleBoard;
import static com.example.a15puzzle.GameLogic.switchButtonText;

public class MainActivity extends AppCompatActivity {
    public TextView textViewScore;
    public TextView textViewTime;

    public Button button1; public Button button2; public Button button3; public Button button4;
    public Button button5; public Button button6; public Button button7; public Button button8;
    public Button button9; public Button button10; public Button button11; public Button button12;
    public Button button13; public Button button14; public Button button15; public Button button16;
    public Button[][] buttonArray;
    public CharSequence[] buttonTexts = new CharSequence[16];
    public LinearLayout gameboardLayout;
    public Button startButton;
    public Stopwatch stopwatch = new Stopwatch();

    // stopwatch functioning
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case TIMER_START:
                    stopwatch.start();
                    mHandler.sendEmptyMessage(TIMER_UPDATE);
                case TIMER_UPDATE:
                    textViewTime.setText("" + stopwatch.getElapsedTimeMin() + ":" + stopwatch.getElapsedTimeSecs());
                    mHandler.sendEmptyMessageDelayed(TIMER_UPDATE, REFRESH_RATE);
                    break;
                case TIMER_STOP:
                    mHandler.removeMessages(TIMER_UPDATE);
                    stopwatch.stop();
                default:
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewScore = findViewById(R.id.textViewScore);
        textViewTime = findViewById(R.id.textViewTime);
        button1 = findViewById(R.id.button1); button9 = findViewById(R.id.button9);
        button2 = findViewById(R.id.button2); button10 = findViewById(R.id.button10);
        button3 = findViewById(R.id.button3); button11 = findViewById(R.id.button11);
        button4 = findViewById(R.id.button4); button12 = findViewById(R.id.button12);
        button5 = findViewById(R.id.button5); button13 = findViewById(R.id.button13);
        button6 = findViewById(R.id.button6); button14 = findViewById(R.id.button14);
        button7 = findViewById(R.id.button7); button15 = findViewById(R.id.button15);
        button8 = findViewById(R.id.button8); button16 = findViewById(R.id.button16);
        buttonArray = new Button[][]{
                {button1, button2, button3, button4},
                {button5, button6, button7, button8},
                {button9, button10, button11, button12},
                {button13, button14, button15, button16}};
        startButton = findViewById(R.id.buttonStart);
        gameboardLayout = findViewById(R.id.gameboardLayout);
        gameboardLayout.setVisibility(LinearLayout.INVISIBLE);

        if (savedInstanceState != null) {
            buttonTexts = savedInstanceState.getCharSequenceArray("buttonTexts");
            long stopwatchCurrentTime = savedInstanceState.getLong("stopwatchTime");
            int moves = savedInstanceState.getInt("moves");
            int i = 0;
            for (Button[] buttons : buttonArray) {
                for (Button button: buttons) {
                    button.setText(buttonTexts[i]);
                    i++;
                }
            }
            stopwatch.currentTime = stopwatchCurrentTime;
            stopwatch.resume();
            mHandler.sendEmptyMessage(TIMER_UPDATE);
            textViewScore.setText("" + moves);
            gameboardLayout.setVisibility(LinearLayout.VISIBLE);
        }


    }

    // start game
    public void buttonStartGame(View view) {
        for (int i = 0; i < SHUFFLE_1000_Times; i++) {
            shuffleBoard(buttonArray);
        }
        gameboardLayout.setVisibility(LinearLayout.VISIBLE);
        mHandler.sendEmptyMessage(TIMER_START);
        GameLogic.resetMoveCounter();
        textViewScore.setText("" + 0);
        startButton.setText(NEW_GAME);

    }

    //gameboard on button click
    public void buttonOnClick(View view)
    {
        textViewScore.setText("" + raiseMovesCounter());
        int buttonId = view.getId();
        if (buttonId == R.id.button1) {
            if (switchButtonText(button1, button2, button5)) {
                checkGameFinish(buttonArray, gameboardLayout, mHandler);
                return;
            }
        } else if (buttonId == R.id.button2) {
            if (switchButtonText(button2, button1, button3, button6)) {
                checkGameFinish(buttonArray, gameboardLayout, mHandler);
                return;
            }
        } else if (buttonId == R.id.button3) {
            if (switchButtonText(button3, button2, button7, button4)) {
                checkGameFinish(buttonArray, gameboardLayout, mHandler);
                return;
            }
        } else if (buttonId == R.id.button4) {
            if (switchButtonText(button4, button3, button8)) {
                checkGameFinish(buttonArray, gameboardLayout, mHandler);
                return;
            }
        } else if (buttonId == R.id.button5) {
            if (switchButtonText(button5, button1, button6, button9)) {
                checkGameFinish(buttonArray, gameboardLayout, mHandler);
                return;
            }
        } else if (buttonId == R.id.button6) {
            if (switchButtonText(button6, button5, button2, button7, button10)) {
                checkGameFinish(buttonArray, gameboardLayout, mHandler);
                return;
            }
        } else if (buttonId == R.id.button7) {
            if (switchButtonText(button7, button3, button8, button6, button11)) {
                checkGameFinish(buttonArray, gameboardLayout, mHandler);
                return;
            }
        } else if (buttonId == R.id.button8) {
            if (switchButtonText(button8, button4, button7, button12)) {
                checkGameFinish(buttonArray, gameboardLayout, mHandler);
                return;
            }
        } else if (buttonId == R.id.button9) {
            if (switchButtonText(button9, button5, button10, button13)) {
                checkGameFinish(buttonArray, gameboardLayout, mHandler);
                return;
            }
        } else if (buttonId == R.id.button10) {
            if (switchButtonText(button10, button6, button9, button11, button14)) {
                checkGameFinish(buttonArray, gameboardLayout, mHandler);
                return;
            }
        } else if (buttonId == R.id.button11) {
            if (switchButtonText(button11, button7, button10, button12, button15)) {
                checkGameFinish(buttonArray, gameboardLayout, mHandler);
                return;
            }
        } else if (buttonId == R.id.button12) {
            if (switchButtonText(button12, button8, button11, button16)) {
                checkGameFinish(buttonArray, gameboardLayout, mHandler);
                return;
            }
        } else if (buttonId == R.id.button13) {
            if (switchButtonText(button13, button9, button14)) {
                checkGameFinish(buttonArray, gameboardLayout, mHandler);
                return;
            }
        } else if (buttonId == R.id.button14) {
            if (switchButtonText(button14, button13, button10, button15)) {
                checkGameFinish(buttonArray, gameboardLayout, mHandler);
                return;
            }
        } else if (buttonId == R.id.button15) {
            if (switchButtonText(button15, button14, button11, button16)) {
                checkGameFinish(buttonArray, gameboardLayout, mHandler);
                return;
            }
        } else if (buttonId == R.id.button16) {
            if (switchButtonText(button16, button12, button15)) {
                checkGameFinish(buttonArray, gameboardLayout, mHandler);
                return;
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Save the values you need into "outState"
        super.onSaveInstanceState(outState);
        int i = 0;
        for (Button[] buttons: buttonArray) {
            for (Button button: buttons) {
                buttonTexts[i] = (button.getText());
                i++;
            }
        }
        outState.putCharSequenceArray("buttonTexts", buttonTexts);
        outState.putLong("stopwatchTime", System.currentTimeMillis() - stopwatch.startTime);
        outState.putInt("moves", Integer.parseInt(textViewScore.getText().toString()));
    }
}
