package com.example.a15puzzle;

import android.os.Handler;
import android.widget.Button;
import android.widget.LinearLayout;
import java.util.ArrayList;
import java.util.Random;
import static com.example.a15puzzle.Constants.TIMER_STOP;

public class GameLogic {

    private static int moveCounter = 0;

    static int raiseMovesCounter() {
        moveCounter++;
        return moveCounter;
    }

    static void resetMoveCounter() {
        moveCounter = 0;
    }

    private static int[] getEmptyTextButtonIndexes(Button[][] array) {
        int[] elementArray = {};
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[i][j].getText().length() == 0) {
                    elementArray = new int[]{i, j};
                }
            }
        }
        return elementArray;
    }

    static void shuffleBoard(Button[][] array) {
        // get empty text button indexes
        int[] indexes = getEmptyTextButtonIndexes(array);
        int yIndex = indexes[0];
        int xIndex = indexes[1];
        Button emptyTextButton = array[yIndex][xIndex];
        // get empty texted button nearby elements and shuffle the board randomly
        ArrayList<Button> closeElements = new ArrayList<>();

        if (yIndex == 0 || yIndex == 3) {
            if (xIndex == 0) {
                closeElements.add(array[yIndex][xIndex + 1]);
            } else if (xIndex == 3) {
                closeElements.add(array[yIndex][xIndex - 1]);
            } else {
                closeElements.add(array[yIndex][xIndex + 1]);
                closeElements.add(array[yIndex][xIndex - 1]);
            }
            if (yIndex == 0) {
                closeElements.add(array[yIndex + 1][xIndex]);
            } else {
                closeElements.add(array[yIndex - 1][xIndex]);
            }
        } else {
            if (xIndex == 0) {
                closeElements.add(array[yIndex][xIndex + 1]);
            } else if (xIndex == 3) {
                closeElements.add(array[yIndex][xIndex - 1]);
            } else {
                closeElements.add(array[yIndex][xIndex + 1]);
                closeElements.add(array[yIndex][xIndex - 1]);
            }
            closeElements.add(array[yIndex + 1][xIndex]);
            closeElements.add(array[yIndex - 1][xIndex]);
        }

        Random random = new Random();
        int randomNumber = random.nextInt(closeElements.size());
        switchButtonText(closeElements.get(randomNumber), emptyTextButton);



    }

    public static void checkGameFinish(Button[][] array, LinearLayout linearLayout, Handler handler) {
        if (array[0][0].getText().equals("1") && array[0][1].getText().equals("2")
                && array[0][2].getText().equals("3") && array[0][3].getText().equals("4")
                && array[1][0].getText().equals("5") && array[1][1].getText().equals("6")
                && array[1][2].getText().equals("7") && array[1][3].getText().equals("8")
                && array[2][0].getText().equals("9") && array[2][1].getText().equals("10")
                && array[2][2].getText().equals("11") && array[2][3].getText().equals("12")
                && array[3][0].getText().equals("13") && array[3][1].getText().equals("14")
                && array[3][2].getText().equals("15") && array[3][3].getText().length() == 0) {
            linearLayout.setVisibility(LinearLayout.INVISIBLE);
            handler.sendEmptyMessage(TIMER_STOP);
        }
    }

    static boolean switchButtonText(Button oldButton, Button newButton) {
        if (newButton.getText().length() == 0) {
            newButton.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        }
        return false;
    }

    static boolean switchButtonText(Button oldButton, Button newButton, Button newButton2) {
        if (newButton.getText().length() == 0) {
            newButton.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        } else if (newButton2.getText().length() == 0) {
            newButton2.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        }
        return false;
    }

    static boolean switchButtonText(Button oldButton, Button newButton, Button newButton2,
                                    Button newButton3) {
        if (newButton.getText().length() == 0) {
            newButton.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        } else if (newButton2.getText().length() == 0) {
            newButton2.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        } else if (newButton3.getText().length() == 0) {
            newButton3.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        }
        return false;
    }

    static boolean switchButtonText(Button oldButton, Button newButton, Button newButton2,
                                    Button newButton3, Button newButton4) {
        if (newButton.getText().length() == 0) {
            newButton.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        } else if (newButton2.getText().length() == 0) {
            newButton2.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        } else if (newButton3.getText().length() == 0) {
            newButton3.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        } else if (newButton4.getText().length() == 0) {
            newButton4.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        }
        return false;
    }

}
