package com.example.a15puzzle;

public class Constants {
    public static final String NEW_GAME = "NEW GAME";
    public static final int SHUFFLE_1000_Times = 1000;
    public static  final int TIMER_START = 0;
    public static  final int TIMER_STOP = 1;
    public static final int TIMER_UPDATE = 2;
    public static final int REFRESH_RATE = 100;
}
