package com.example.a15puzzle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.util.Arrays;
import java.util.LinkedList;

import static com.example.a15puzzle.GameLogic.getButtonIndexes;
import static com.example.a15puzzle.GameLogic.switchButtonText;

public class MainActivity extends AppCompatActivity {
    public TextView textViewScore;
    public TextView textViewTime;
    public Button button1; public Button button2; public Button button3; public Button button4;
    public Button button5; public Button button6; public Button button7; public Button button8;
    public Button button9; public Button button10; public Button button11; public Button button12;
    public Button button13; public Button button14; public Button button15; public Button button16;



    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewScore = findViewById(R.id.textViewScore);
        textViewTime = findViewById(R.id.textViewTime);
        button1 = findViewById(R.id.button1); button9 = findViewById(R.id.button9);
        button2 = findViewById(R.id.button2); button10 = findViewById(R.id.button10);
        button3 = findViewById(R.id.button3); button11 = findViewById(R.id.button11);
        button4 = findViewById(R.id.button4); button12 = findViewById(R.id.button12);
        button5 = findViewById(R.id.button5); button13 = findViewById(R.id.button13);
        button6 = findViewById(R.id.button6); button14 = findViewById(R.id.button14);
        button7 = findViewById(R.id.button7); button15 = findViewById(R.id.button15);
        button8 = findViewById(R.id.button8); button16 = findViewById(R.id.button16);

    }

    public void buttonOnClick(View view)
    {
        int buttonId = view.getId();
        if (buttonId == R.id.button1) {
            if (switchButtonText(button1, button2, button5)) {return;}
        } else if (buttonId == R.id.button2) {
            if (switchButtonText(button2, button3, button6)) {return;}
        } else if (buttonId == R.id.button3) {
            if (switchButtonText(button3, button2, button7, button4)) {return;}
        } else if (buttonId == R.id.button4) {
            if (switchButtonText(button4, button3, button8)) {return;}
        } else if (buttonId == R.id.button5) {
            if (switchButtonText(button5, button1, button6, button9)) {return;}
        } else if (buttonId == R.id.button6) {
            if (switchButtonText(button6, button5, button2, button7, button10)) {return;}
        } else if (buttonId == R.id.button7) {
            if (switchButtonText(button7, button3, button8, button6, button11)) {return;}
        } else if (buttonId == R.id.button8) {
            if (switchButtonText(button8, button4, button7, button12)) {return;}
        } else if (buttonId == R.id.button9) {
            if (switchButtonText(button9, button5, button10, button13)) {return;}
        } else if (buttonId == R.id.button10) {
            if (switchButtonText(button10, button6, button9, button11, button14)) {return;}
        } else if (buttonId == R.id.button11) {
            if (switchButtonText(button11, button7, button10, button12, button15)) {return;}
        } else if (buttonId == R.id.button12) {
            if (switchButtonText(button12, button8, button11, button16)) {return;}
        } else if (buttonId == R.id.button13) {
            if (switchButtonText(button13, button9, button11)) {return;}
        } else if (buttonId == R.id.button14) {
            if (switchButtonText(button14, button13, button10, button15)) {return;}
        } else if (buttonId == R.id.button15) {
            if (switchButtonText(button15, button14, button11, button16)) {return;}
        } else if (buttonId == R.id.button16) {
            if (switchButtonText(button16, button12, button15)) {return;}
        }
    }
}
