package com.example.a15puzzle;

import android.widget.Button;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Map;

public class GameLogic {

    public static int[] getButtonIndexes(int[][] array, int element) {
        int[] elementArray = {};
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[i][j] == element) {
                    elementArray = new int[]{i, j};
                }
            }
        }
        return elementArray;
    }

    public static boolean switchButtonText(Button oldButton, Button newButton) {
        if (newButton.getText().length() == 0) {
            newButton.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        }
        return false;
    }

    public static boolean switchButtonText(Button oldButton, Button newButton, Button newButton2) {
        if (newButton.getText().length() == 0) {
            newButton.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        } else if (newButton2.getText().length() == 0) {
            newButton2.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        }
        return false;
    }

    public static boolean switchButtonText(Button oldButton, Button newButton, Button newButton2,
                                           Button newButton3) {
        if (newButton.getText().length() == 0) {
            newButton.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        } else if (newButton2.getText().length() == 0) {
            newButton2.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        } else if (newButton3.getText().length() == 0) {
            newButton3.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        }
        return false;
    }

    public static boolean switchButtonText(Button oldButton, Button newButton, Button newButton2,
                                           Button newButton3, Button newButton4) {
        if (newButton.getText().length() == 0) {
            newButton.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        } else if (newButton2.getText().length() == 0) {
            newButton2.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        } else if (newButton3.getText().length() == 0) {
            newButton3.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        } else if (newButton4.getText().length() == 0) {
            newButton4.setText(oldButton.getText());
            oldButton.setText("");
            return true;
        }
        return false;
    }

}
