package com.example.radio;

public final class C {
    static private final String prefix = "com.example.radio.";
    static final String SERVICE_MEDIA_SOURCE_KEY = prefix + "serviceMediaSourceIntent";
    static final String SERVICE = prefix + "service";
    static final String STOP_SERVICE = "STOP";
    static final int MUSICSERVICE_STOPPED = 0;
    static final int MUSICSERVICE_BUFFERING = 1;
    static final int MUSICSERVICE_PLAYING = 2;

    static final String BASE_URL = "http://sky.babahhcdn.com/";
    static final String API_BASE_URL = "http://dad.akaver.com/api/SongTitles/";
    static final String NRJ_RADIO = BASE_URL + "NRJ";
    static final String SKYPLUS_RADIO = BASE_URL + "SKYPLUS";
    static final String RRAP_RADIO = BASE_URL + "rrap";
    static final String RETRO_RADIO = BASE_URL + "RETRO";
    static final String NRJ = "NRJ";
    static final String SKYPLUS = "SP";
    static final String RRAP = "RRAP";
    static final String RETRO = "RETRO";
    static final String SKYPLUS_LONG = "SKYPLUS";


    static String INFO_URL = "";
    static String CURRENT_RADIO = "NRJ";
    static String CURRENT_RADIO_STATISTICS;

    static final String MUSICSERVICE_INTENT_PLAYING = prefix + "MUSICSERVICE_INTENT_PLAYING";
    static final String MUSICSERVICE_INTENT_BUFFERING = prefix + "MUSICSERVICE_INTENT_BUFFERING";
    static final String MUSICSERVICE_INTENT_STOPPED = prefix + "MUSICSERVICE_INTENT_STOPED";

    static final String MUSICSERVICE_INTENT_SONGINFO = prefix + "MUSICSERVICE_INTENT_SONGINFO";

    static final String MUSICSERVICE_INTENT_ARTIST= prefix + "ARTIST";
    static final String MUSICSERVICE_INTENT_TRACKTITLE = prefix + "TRACKTITLE";

    // Activity to Mediaplayer broadcast intent messages
    static public final String ACTIVITY_INTENT_STARTMUSIC = prefix + "ACTIVITY_INTENT_STARTMUSIC";
    static public final String ACTIVITY_INTENT_STOPPMUSIC = prefix + "ACTIVITY_INTENT_STOPPMUSIC";

    // Main activity PLAY button labels
    static final String BUTTONCONTROLMUSIC_LABEL_PLAYING = "STOP";
    static final String BUTTONCONTROLMUSIC_LABEL_BUFFERING = "BUFFERING";
    static final String BUTTONCONTROLMUSIC_LABEL_STOPPED = "PLAY";


    static final String MUSICSERVICE_VOLLEYTAG = prefix + "MUSICSERVICE_VOLLEYTAG";

}
