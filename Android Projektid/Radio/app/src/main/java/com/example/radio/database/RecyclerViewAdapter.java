package com.example.radio.database;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.radio.R;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private String[] songInfoStringsList;
    private LayoutInflater inflater;
    private ItemClickListener itemClickListener;


    public RecyclerViewAdapter(Context context, String[] songInfoList) {
        this.songInfoStringsList = songInfoList;
        inflater = LayoutInflater.from(context); // we need to unpack our xml layouts
    }

    // inflate the view from xml layout when needed
    @NonNull
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View rowView = inflater.inflate(R.layout.recycler_songs_row, viewGroup, false);
        return new ViewHolder(rowView);
    }

    // binds the data to the view elements in each row
    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter.ViewHolder viewHolder, int i) {
        viewHolder.songInfo.setText(songInfoStringsList[i]);

    }

    @Override
    public int getItemCount() {
        // how many rows there will be
        int i = 0;
        for (String songInfo: songInfoStringsList) {
            i++;
        }
        return i;
    }

    // stores and recycles views as they are scrolled off the screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView songInfo;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            songInfo = itemView.findViewById(R.id.songInfo);

            itemView.setOnClickListener(this); //ViewHolder.onClick

        }

        @Override
        public void onClick(View v) {
            if (itemClickListener != null){
                itemClickListener.onRecyclerViewRowClick(v, getAdapterPosition());
            }

        }


    }

    // whoever uses this recycler has to implement ItemClickListener interface and set itself up as our listener
    void setItemClickListener(ItemClickListener itemClickListener){
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener{
        void onRecyclerViewRowClick(View view, int position);
    }

}