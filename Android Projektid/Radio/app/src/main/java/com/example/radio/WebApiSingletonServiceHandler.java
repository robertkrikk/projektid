package com.example.radio;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class WebApiSingletonServiceHandler {

    private static WebApiSingletonServiceHandler mInstance;
    private static Context mContext;
    private static String VolleyTAG = "com.itcollege.volley";
    private RequestQueue mRequestQueue;

    private WebApiSingletonServiceHandler(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();
    }

    /// Singleton pattern
    public static synchronized WebApiSingletonServiceHandler getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new WebApiSingletonServiceHandler(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request <T> request) {
        request.addMarker(VolleyTAG);
        getRequestQueue().add(request);
    }

    public void cancelRequestQueue() {
        getRequestQueue().cancelAll(VolleyTAG);
    }


}
