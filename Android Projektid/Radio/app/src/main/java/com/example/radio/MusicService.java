package com.example.radio;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.radio.database.SongInfo;
import com.example.radio.database.SongInfoRepository;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.example.radio.MainActivity.mSongInfoRepository;

public class MusicService extends Service implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
    private static final String TAG = MusicService.class.getSimpleName();
    private MediaPlayer mMediaPlayer = new MediaPlayer();
    private String mMediaSource = "";
    private String service = "";
    private ScheduledExecutorService scheduledExecutorService;

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        super.onCreate();
        mMediaPlayer.setOnCompletionListener(this);
        mMediaPlayer.setOnErrorListener(this);
        mMediaPlayer.setOnPreparedListener(this);
        mMediaPlayer.reset();
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");

        if (intent == null) {
            Log.d(TAG, "Intent is null!!!!");
            return Service.START_NOT_STICKY;
        }
        service = intent.getExtras().getString(C.SERVICE);

        mMediaPlayer.reset();
        mMediaSource = intent.getExtras().getString(C.SERVICE_MEDIA_SOURCE_KEY);
        // TODO: Handle incoming phone calls, skype etc...


        try {
            mMediaPlayer.setDataSource(mMediaSource);

            WebApiSingletonServiceHandler.getInstance(getApplicationContext()).cancelRequestQueue();
            mMediaPlayer.prepareAsync();
            // TODO: inform main activity that we are buffering

            Intent intentInformActivity = new Intent(C.MUSICSERVICE_INTENT_BUFFERING);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intentInformActivity);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Service.START_REDELIVER_INTENT;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.d(TAG, "onCompletion");

    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.d(TAG, "onError");

        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.d(TAG, "onPrepared");
        mMediaPlayer.start();

        //TODO: Inform main activity, that we are playing.
        Intent intentInformActivity = new Intent(C.MUSICSERVICE_INTENT_PLAYING);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intentInformActivity);

        //TODO: Start song info gathering

        StartMediaInfoGathering();
    }

    @Override
    public void onDestroy() {
        mMediaPlayer.stop();
        Intent intentInformActivity = new Intent(C.MUSICSERVICE_INTENT_STOPPED);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intentInformActivity);
        scheduledExecutorService.shutdown();
        super.onDestroy();
    }

    private void StartMediaInfoGathering() {
        scheduledExecutorService = Executors.newScheduledThreadPool(5);
        scheduledExecutorService.scheduleAtFixedRate(
                new Runnable() {
                    @Override
                    public void run() {
                        new GetSongInfo().execute();
                    }
                }, 0 , 30, TimeUnit.SECONDS
        );
    }

    private class GetSongInfo extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            final String url = C.INFO_URL;
            final StringRequest stringRequest = new StringRequest(
                    Request.Method.GET,
                    url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d(TAG, "onResponse" + response);
                            //PARSE THE JSON RESPONSE
                            try {
                                JSONObject jsonObjectStationInfo = new JSONObject(response);
                                JSONArray jsonArraySongHistory  = jsonObjectStationInfo.getJSONArray("SongHistoryList");
                                JSONObject jsonArraySongInfo = jsonArraySongHistory.getJSONObject(0);
                                String artist = jsonArraySongInfo.getString("Artist");
                                String trackTitle = jsonArraySongInfo.getString("Title");
                                mSongInfoRepository.add(new SongInfo(C.CURRENT_RADIO, artist, trackTitle));

                                //broadcast the song info
                                Intent sendSongInfoIntent = new Intent(C.MUSICSERVICE_INTENT_SONGINFO);
                                sendSongInfoIntent.putExtra(C.MUSICSERVICE_INTENT_ARTIST, artist);
                                sendSongInfoIntent.putExtra(C.MUSICSERVICE_INTENT_TRACKTITLE, trackTitle);
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(sendSongInfoIntent);

                            } catch (JSONException e) {
                                Log.d(TAG, e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "onErrorResponse" + error.getMessage());
                }
            }
            );

            stringRequest.addMarker(C.MUSICSERVICE_VOLLEYTAG);
            // ADD THE REQUEST TO VOLLEY QUEUE
            WebApiSingletonServiceHandler.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
            return null;
        }
    }


}
