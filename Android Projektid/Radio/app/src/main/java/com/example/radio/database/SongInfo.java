package com.example.radio.database;

public class SongInfo {

    public int id;
    public String artist;
    public String songName;
    public String station;

    public SongInfo(String station, String artist, String songName){
        // call constructor overload
        this(0, station,  artist, songName);
    }

    SongInfo(int id, String station, String artist, String songName){
        this.id = id;
        this.artist = artist;
        this.station = station;
        this.songName = songName;
    }

    @Override
    public String toString() {
        return station + "-" + artist + "-" + songName;
    }
}
