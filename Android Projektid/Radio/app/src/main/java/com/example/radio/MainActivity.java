package com.example.radio;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.radio.database.SongInfo;
import com.example.radio.database.SongInfoRepository;

import java.util.List;

import static com.example.radio.C.API_BASE_URL;
import static com.example.radio.C.BUTTONCONTROLMUSIC_LABEL_PLAYING;
import static com.example.radio.C.CURRENT_RADIO;
import static com.example.radio.C.CURRENT_RADIO_STATISTICS;
import static com.example.radio.C.NRJ;
import static com.example.radio.C.RETRO;
import static com.example.radio.C.RRAP;
import static com.example.radio.C.SKYPLUS;
import static com.example.radio.C.SKYPLUS_LONG;

public class MainActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener, AdapterView.OnItemSelectedListener {
    static private final String TAG = MainActivity.class.getSimpleName();
    private BroadcastReceiver mBroadcastReceiver;
    private int mMusicPlayerStatus = C.MUSICSERVICE_STOPPED;
    private Button mButtonControlMusic, mButtonVolumeMinus, mButtonVolumePlus, mButtonStatistics;
    private TextView mTextViewArtist, mTextViewTitle;
    private SeekBar mSeekBarAudioVolume;
    private SettingsContentObserver mSettingsContentObserver;
    private String mArtist, mTrackTitle, mRadioStation;
    private Spinner mSpinnerRadioStations;
    public static SongInfoRepository mSongInfoRepository;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mButtonControlMusic = (Button) findViewById(R.id.buttonControlMusic);
        mTextViewArtist =(TextView) findViewById(R.id.textViewArtist);
        mTextViewTitle =(TextView) findViewById(R.id.textViewTitle);
        mSeekBarAudioVolume = (SeekBar)  findViewById(R.id.seekBarAudioVolume);
        mSpinnerRadioStations = (Spinner) findViewById(R.id.spinner);
        mButtonStatistics = (Button) findViewById(R.id.buttonStatistics);
        mButtonVolumeMinus = (Button) findViewById(R.id.buttonVolumeMinus);
        mButtonVolumePlus = (Button) findViewById(R.id.buttonVolumePlus);


        // Stations spinner setup
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.Stations,
                android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerRadioStations.setAdapter(adapter);
        mSpinnerRadioStations.setOnItemSelectedListener(this);

        // DB setup -
        mSongInfoRepository = new SongInfoRepository(this);
        mSongInfoRepository.open();





        //// set the seek bar maximum -based on audiomanager max
        AudioManager audio = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        mSeekBarAudioVolume.setMax(audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        //// and the position
        mSeekBarAudioVolume.setProgress(audio.getStreamVolume(AudioManager.STREAM_MUSIC));
        mSeekBarAudioVolume.setOnSeekBarChangeListener(this);
        // Register intents we intend to receive
        // this is explicit - ie we can receive only those we specify. this is mandatory.
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(C.MUSICSERVICE_INTENT_BUFFERING);
        intentFilter.addAction(C.MUSICSERVICE_INTENT_PLAYING);
        intentFilter.addAction(C.MUSICSERVICE_INTENT_STOPPED);
        intentFilter.addAction(C.MUSICSERVICE_INTENT_SONGINFO);

        mBroadcastReceiver = new MainActivityBroadcastReceiver();

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mBroadcastReceiver, intentFilter);
        mButtonVolumePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AudioManager audio = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
                int currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
                audio.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume + 1, AudioManager.FLAG_SHOW_UI);

            }
        });

        mButtonVolumeMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AudioManager audio = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
                int currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
                audio.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume - 1, AudioManager.FLAG_SHOW_UI);
            }
        });

        mButtonStatistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, StatisticsActivity.class);
                startActivity(intent);
            }
        });
    }


    // ============================== LIFECYCLE EVENTS ===============================
    @Override
    protected void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        mSettingsContentObserver = new SettingsContentObserver(new Handler());
        this.getApplicationContext().getContentResolver().registerContentObserver(
                Settings.System.CONTENT_URI, true, mSettingsContentObserver);
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
    }


    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }


    @Override
    protected void onRestart() {
        Log.d(TAG, "onRestart");
        super.onRestart();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "onRestoreInstanceState");
        super.onRestoreInstanceState(savedInstanceState);
    }


    public void buttonControlMusicOnClick(View view) {
        Log.d(TAG, "buttonControlMusicOnClick");
        Button button = (Button) findViewById(view.getId());
        if (button.getText().equals(BUTTONCONTROLMUSIC_LABEL_PLAYING)) {
            Intent intentStopService = new Intent(this, MusicService.class);
            mMusicPlayerStatus = C.MUSICSERVICE_STOPPED;
            UpdateUI();
            stopService(intentStopService);
        } else {
            Intent intentStartService = new Intent(this, MusicService.class);
            intentStartService.putExtra(C.SERVICE_MEDIA_SOURCE_KEY, mRadioStation);
            this.startService(intentStartService);
        }
    }

    public void UpdateUI(){
        switch (mMusicPlayerStatus){
            case C.MUSICSERVICE_STOPPED:
                mButtonControlMusic.setText(C.BUTTONCONTROLMUSIC_LABEL_STOPPED);
                mArtist = "";
                mTrackTitle = "";
                break;
            case C.MUSICSERVICE_BUFFERING:
                mButtonControlMusic.setText(C.BUTTONCONTROLMUSIC_LABEL_BUFFERING);
                break;
            case C.MUSICSERVICE_PLAYING:
                mButtonControlMusic.setText(BUTTONCONTROLMUSIC_LABEL_PLAYING);
                break;

        }
        mTextViewArtist.setText(mArtist);
        mTextViewTitle.setText(mTrackTitle);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        AudioManager audio = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        audio.setStreamVolume(AudioManager.STREAM_MUSIC, progress,AudioManager.FLAG_SHOW_UI);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    // Change radio stations
    @Override
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        String item = parent.getItemAtPosition(pos).toString();
        if (!item.equals(CURRENT_RADIO)) {
            Intent intentStopService = new Intent(this, MusicService.class);
            mMusicPlayerStatus = C.MUSICSERVICE_STOPPED;
            UpdateUI();
            stopService(intentStopService);
        }
        if (item.equals("RRAP")) {
            mRadioStation = C.RRAP_RADIO;
            C.INFO_URL = API_BASE_URL + RRAP;
            CURRENT_RADIO = RRAP;
            CURRENT_RADIO_STATISTICS = RRAP;
        } else if (item.equals("NRJ")) {
            mRadioStation = C.NRJ_RADIO;
            C.INFO_URL = API_BASE_URL + NRJ;
            CURRENT_RADIO = NRJ;
            CURRENT_RADIO_STATISTICS = NRJ;
        } else if (item.equals("RETRO")) {
            mRadioStation = C.RETRO_RADIO;
            C.INFO_URL = API_BASE_URL + RETRO;
            CURRENT_RADIO = RETRO;
            CURRENT_RADIO_STATISTICS = RETRO;
        } else if (item.equals("SKYPLUS")) {
            mRadioStation = C.SKYPLUS_RADIO;
            C.INFO_URL = API_BASE_URL + SKYPLUS;
            CURRENT_RADIO = SKYPLUS;
            CURRENT_RADIO_STATISTICS = SKYPLUS_LONG;
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    public class MainActivityBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive " + intent.getAction());
            switch (intent.getAction()){
                case C.MUSICSERVICE_INTENT_BUFFERING:
                    mMusicPlayerStatus = C.MUSICSERVICE_BUFFERING;
                    break;
                case C.MUSICSERVICE_INTENT_PLAYING:
                    mMusicPlayerStatus = C.MUSICSERVICE_PLAYING;
                    break;
                case C.MUSICSERVICE_INTENT_STOPPED:
                    mMusicPlayerStatus = C.MUSICSERVICE_STOPPED;
                    break;
                case C.MUSICSERVICE_INTENT_SONGINFO:
                    mArtist = intent.getStringExtra(C.MUSICSERVICE_INTENT_ARTIST);
                    mTrackTitle = intent.getStringExtra(C.MUSICSERVICE_INTENT_TRACKTITLE);
                    break;
            }
            UpdateUI();
        }
    }



    public class SettingsContentObserver extends ContentObserver{
        private int previousVolume = -1;

        public SettingsContentObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            Log.d(TAG, "SettingsContentObserver");

            super.onChange(selfChange);

            AudioManager audio = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            int currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
            Log.d(TAG, "Volume" + Integer.toString(currentVolume));

            if (currentVolume != previousVolume) {
                previousVolume = currentVolume;
                mSeekBarAudioVolume.setProgress(currentVolume);
            }

        }
    }

}
