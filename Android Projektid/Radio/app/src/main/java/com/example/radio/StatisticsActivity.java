package com.example.radio;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.example.radio.database.RecyclerViewAdapter;
import com.example.radio.database.SongInfoRepository;

public class StatisticsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, RecyclerViewAdapter.ItemClickListener {
    private Spinner mSpinnerStationStatictics;
    private RecyclerViewAdapter recyclerViewAdapter;
    private SongInfoRepository songInfoRepository;
    private String[] songInfoList;
    private TextView mTextViewUniqueArtistCount;
    private TextView mTextViewUniqueSongsCount;
    private int spinnerPosition;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistics);
        mSpinnerStationStatictics = (Spinner) findViewById(R.id.stationSpinner);
        mTextViewUniqueArtistCount = findViewById(R.id.textViewUniqueArtists);
        mTextViewUniqueSongsCount = findViewById(R.id.textViewUniqueSongs);

        // Stations spinner setup
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.Stations,
                android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerStationStatictics.setAdapter(adapter);
        mSpinnerStationStatictics.setOnItemSelectedListener(this);
        spinnerPosition = adapter.getPosition(C.CURRENT_RADIO_STATISTICS);
        mSpinnerStationStatictics.setSelection(spinnerPosition);


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        if (item.equals("RRAP")) {
            C.CURRENT_RADIO_STATISTICS = C.RRAP;
        } else if (item.equals("NRJ")) {
            C.CURRENT_RADIO_STATISTICS = C.NRJ;
        } else if (item.equals("RETRO")) {
            C.CURRENT_RADIO_STATISTICS = C.RETRO;
        } else if (item.equals("SKYPLUS")) {
            C.CURRENT_RADIO_STATISTICS = C.SKYPLUS;
        }
        songInfoRepository = MainActivity.mSongInfoRepository;
        songInfoList = songInfoRepository.getSongInfo(C.CURRENT_RADIO_STATISTICS);
        mTextViewUniqueArtistCount.setText(Integer.toString(songInfoRepository
                .getUniqueArtistCount(C.CURRENT_RADIO_STATISTICS)));
        mTextViewUniqueSongsCount.setText(Integer.toString(songInfoRepository.
                getUniqueSongCount(C.CURRENT_RADIO_STATISTICS)));
        RecyclerView recyclerView = findViewById(R.id.recyclerViewSongInfo);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewAdapter = new RecyclerViewAdapter(this, songInfoList);
        recyclerView.setAdapter(recyclerViewAdapter);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onRecyclerViewRowClick(View view, int position) {
        Toast.makeText(this, "You clicked on "  + Integer.toString(position), Toast.LENGTH_LONG).show();
    }
}
