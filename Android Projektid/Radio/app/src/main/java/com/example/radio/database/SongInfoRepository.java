package com.example.radio.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;

public class SongInfoRepository {
    private DatabaseHelper dbHelper;
    private Context context;
    private SQLiteDatabase db;

    public SongInfoRepository(Context context) {
        this.context = context;
    }

    public SongInfoRepository open(){
        dbHelper = new DatabaseHelper(context);
        db = dbHelper.getWritableDatabase();
        return this;
    }

    public void close(){
        dbHelper.close();
    }

    public void add(SongInfo song){
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.RADIO_STATION, song.station);
        contentValues.put(DatabaseHelper.ARTIST_NAME, song.artist);
        contentValues.put(DatabaseHelper.SONG_NAME, song.songName);
        db.insert(DatabaseHelper.SONG_INFO_TABLE_NAME,null, contentValues);
    }

    public void clearDatabase() {
        db.execSQL("delete from " + DatabaseHelper.SONG_INFO_TABLE_NAME);
    }


    private Cursor fetch(){
        String[] columns = new String[]{
                DatabaseHelper.SONG_INFO_ID,
                DatabaseHelper.RADIO_STATION,
                DatabaseHelper.ARTIST_NAME,
                DatabaseHelper.SONG_NAME
        };
        Cursor cursor = db.query(DatabaseHelper.SONG_INFO_TABLE_NAME, columns,
                null,null,null, null, null);
        // is this really neccesary?
        if (cursor != null){
            cursor.moveToFirst();
        }

        return cursor;
    }

    public int getUniqueArtistCount(String radioStation){
        int i = 0;
        String[] columns = new String[]{radioStation};
        Cursor cursor = db.rawQuery("SELECT DISTINCT " +
                DatabaseHelper.ARTIST_NAME + " FROM " +
                DatabaseHelper.SONG_INFO_TABLE_NAME + " WHERE " +
                DatabaseHelper.RADIO_STATION + " = ?", columns);
        return cursor.getCount();
    }

    public int getUniqueSongCount(String radioStation) {
        int i = 0;
        String[] columns = new String[]{radioStation};
        Cursor cursor = db.rawQuery("SELECT DISTINCT " +
                DatabaseHelper.SONG_NAME + " FROM " +
                DatabaseHelper.SONG_INFO_TABLE_NAME + " WHERE " +
                DatabaseHelper.RADIO_STATION + " = ?", columns);
        return cursor.getCount();
    }


    public int getSongCount(String songName, String artistName, String radioStation) {
        String[] columns = new String[]{radioStation, artistName, songName};
        Cursor cursor = db.rawQuery("SELECT " +
                DatabaseHelper.SONG_NAME + " FROM " +
                DatabaseHelper.SONG_INFO_TABLE_NAME + " WHERE " +
                DatabaseHelper.RADIO_STATION + " = ? AND " +
                DatabaseHelper.ARTIST_NAME + " = ? AND " +
                DatabaseHelper.SONG_NAME + " = ?", columns);
        return cursor.getCount();
    }

    public String[] getSongInfo(String radioStation) {
        ArrayList<String> songInfoAndCount = new ArrayList<>();
        String[] columns = new String[]{radioStation};
        Cursor cursor = db.rawQuery("SELECT DISTINCT " +
                DatabaseHelper.ARTIST_NAME + ", " + DatabaseHelper.SONG_NAME + ", " +
                DatabaseHelper.RADIO_STATION + " FROM " +
                DatabaseHelper.SONG_INFO_TABLE_NAME + " WHERE " +
                DatabaseHelper.RADIO_STATION + " = ?", columns);
        if (cursor.moveToFirst()) {
            do{
                String artist = cursor.getString(cursor.getColumnIndex(DatabaseHelper.ARTIST_NAME));
                String song = cursor.getString(cursor.getColumnIndex(DatabaseHelper.SONG_NAME));
                int count = getSongCount(song, artist, radioStation);
                songInfoAndCount.add(
                       artist + " - " + song + "  - x" + Integer.toString(count)
               );
            } while (cursor.moveToNext());
        }
        String[] songInfoList = new String[songInfoAndCount.size()];
        int counter = 0;
        for (String songInfo: songInfoAndCount) {
            songInfoList[counter] = songInfo;
            counter++;
        }
        return songInfoList;
    }


    public List<SongInfo> getAll(){
        ArrayList<SongInfo> songInfos =  new ArrayList<>();
        Cursor cursor = fetch();
        if (cursor.moveToFirst()){
            do{
                songInfos.add(new SongInfo(
                        cursor.getInt(cursor.getColumnIndex(DatabaseHelper.SONG_INFO_ID)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.RADIO_STATION)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.ARTIST_NAME)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHelper.SONG_NAME))));
            } while (cursor.moveToNext());
        }
        return songInfos;
    }

}
