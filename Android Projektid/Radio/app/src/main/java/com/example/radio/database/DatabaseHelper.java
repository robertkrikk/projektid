package com.example.radio.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "SONGINFO.DB";
    public static final int DB_VERSION = 1;

    public static final String SONG_INFO_TABLE_NAME = "songInfo";

    public static final String SONG_INFO_ID = "_id";
    public static final String RADIO_STATION = "radioStation";
    public static final String ARTIST_NAME = "artistName";
    public static final String SONG_NAME = "songName";

    public static final String SONG_INFO_CREATE_TABLE_SQL = "create table " + SONG_INFO_TABLE_NAME +
            "(" +
            SONG_INFO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            RADIO_STATION + " TEXT NOT NULL, " +
            ARTIST_NAME + " TEXT NOT NULL, " +
            SONG_NAME + " TEXT NOT NULL);";


    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SONG_INFO_CREATE_TABLE_SQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO: Migrate data to new table structure
        db.execSQL("DROP TABLE IF EXISTS " + SONG_INFO_TABLE_NAME);
        onCreate(db);
    }

}
