Crypto Clicker
========

Coinile clickides kasvab mängija coinide arv. Coinide eest on võimalik osta erinevaid upgradesid mis omakorda aitavad mängija coinide arvu kasvatada. Mängul otsest eesmärki ei ole, aga on olemas achievementid mida saavutada. Idee on võetud Cookie Clickeri pealt http://orteil.dashnet.org/cookieclicker/

Liikmed
--------

- Ra Koort (rakoor)
- Robert Krikk (rokrik)
- Dan Siimson (dasiim)

Funktsionaalsus
---------------

- Põhicoini saab clickida, et panka kasvatada
- Võimalik on osta upgradesid, et tekiks passiivne sissetulek
- Achievementid mille poole pürgida
- Ajutised powerupid (Läheb upgradede alla)
- Statistikavaade
- Progressi salvestamine

Ekraanivaated
-------------

**Mäng**

App alustab alati mänguvaatest. Ekraani keskel asetseb Coin (suur coin mida saab tapida e. clickida). Ekraani ülemises osas näeb hetkelist coini väärtust, mining powerit ja oma panga väärtust. Lisaks on ekraani nurgas menüü kust pääseb ligi lehtedele

	- Stats
	- Upgrades
	- Settings
	- Achievements
	- (Highscores)

**Stats**

Siin on ekraanil näha lihtsalt statistikat jooksva mängu kohta. Näiteks võiks seal näha olla kogu mängu jooksul tehtud coinide arv, hetkeline mining power, ostetud minerite ja upgradede arv, coini väärtus jne.

**Upgrades**

See on nn. poeekraan kus saab upgradesid näha ja neid teatud raha eest juurde osta.

**Settings**

Lihtne settings menüü kus saab seadistada coini nime ja pildi ning soovi korral heli maha keerata.

**Achievements**

Lihtne vaade kus saab näha achievemente. (Locked või unlocked)


Plaan
-----

- \8. nädal: Algse funktsionaalsuse realiseerimine (Mängu ja Settings ekraanivaade)
- \9. nädal: Upgrades, Stats ja Achievements realiseerimine (Upgrades, Achievements ja Stats erkaanivaated)
- \10. nädal: Upgradede ja Achievementide lisamine
- \11. nädal: UI kujundus ja viimistlused
- \12. nädal: Esitamine
- \13. nädal: paranduste tegemine, 
- \14. nädal: JAR-komplekteerimine
- \15. nädal: parandused/täiendused
- \16. nädal: Lõplik esitamine

Kasutatav tehnoloogia
----------------------

Android

Punktisoov
----------

8 punkti