package teamrn.promo;

import teamrn.bank.Bank;

import static teamrn.bank.Bank.getMoney;
import static teamrn.bank.Bank.setMoney;

public class Promo {

    public static class FacebookAdPromo {
        private static double promoPrice = 10000;
        private static double raise = 4;

        public static double getPromoPrice() {
            return promoPrice;
        }

        public static double getRaise() {
            return raise;
        }

        public static boolean buyPromo() {
            if (getMoney() >= getPromoPrice()) {
                setMoney(getMoney() - getPromoPrice());
                Bank.setCoinrate(Bank.getCoinrate() * raise);
                return true;
            }
            return false;
        }
    }

    public static class TwitterPromo {
        private static double promoPrice = 5000;
        private static double raise = 2;

        public static double getPromoPrice() {
            return promoPrice;
        }

        public static double getRaise() {
            return raise;
        }

        public static boolean buyPromo() {
            if (getMoney() >= getPromoPrice()) {
                setMoney(getMoney() - getPromoPrice());
                Bank.setCoinrate(Bank.getCoinrate() * raise);
                return true;
            }
            return false;
        }
    }

    public static class HDTanelPromo {
        private static double promoPrice = 1000;

        public static double getPromoPrice() {
            return promoPrice;
        }

        public static boolean buyPromo() {
            if (getMoney() >= getPromoPrice()) {
                setMoney(getMoney() - getPromoPrice());
                return true;
            }
            return false;
        }
    }


}
