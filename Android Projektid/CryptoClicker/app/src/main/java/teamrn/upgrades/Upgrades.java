package teamrn.upgrades;

import teamrn.bank.Bank;
import teamrn.cryptoclickerxd.MainActivity;

public class Upgrades {

    public Upgrades() { }

    
    public static class GPU {
        private static int gpuCounter = 0;
        private static double coinIncrease = 0.1;
        private static double poolCoinIncrease = 5;
        private static int poolCounter = 0;
        private static double upgradePrice = 400;
        
        private static void setUpgradePrice(double price) {
            upgradePrice = price;
        }
        
        public static double getUpgradePrice() {
            return upgradePrice;
        }

        public static int getPoolCounter() {
            return poolCounter;
        }

        public static int getGpuCounter() {
            return gpuCounter;
        }

        public static boolean buyGPU() {
            if (Bank.getMoney() >= getUpgradePrice()) {
                Bank.decreaseMoney(getUpgradePrice());
                setUpgradePrice(MainActivity.round(Math.pow(getUpgradePrice(), 1.05), 2));
                gpuCounter += 1;
                MainActivity.setCps(MainActivity.getCps() + coinIncrease);
                return true;
            }
            return false;
        }

        public static boolean createPool() {
            if (gpuCounter >= 10 && Bank.getMoney() >= 1500 && poolCounter == 0) {
                Bank.decreaseMoney( 1500);
                poolCounter += 1;
                MainActivity.setCps(MainActivity.getCps() + poolCoinIncrease);
                return true;
            }
            return false;
        }

        public static double getCoinIncrease() {
            return coinIncrease;
        }

        public static double getPoolCoinIncrease() {
            return poolCoinIncrease;
        }

    }

    public static class ASIC {
        private static int asicCounter = 0;
        private static double coinIncrease = 0.5;
        private static int poolCoinIncrease = 15;
        private static int poolCounter = 0;
        private static double upgradePrice = 1000;

        private static void setUpgradePrice(double price) {
            upgradePrice = price;
        }

        public static double getUpgradePrice() {
            return upgradePrice;
        }

        public static int getPoolCounter() {
            return poolCounter;
        }

        public static int getAsicCounter() {
            return asicCounter;
        }

        public static boolean buyASIC() {
            if (Bank.getMoney() >= getUpgradePrice()) {
                Bank.decreaseMoney(getUpgradePrice());
                setUpgradePrice(MainActivity.round(Math.pow(getUpgradePrice(), 1.05), 2));
                asicCounter += 1;
                MainActivity.setCps(MainActivity.getCps() + coinIncrease);
                return true;
            }
            return false;
        }

        public static boolean createPool() {
            if (asicCounter >= 10 && Bank.getMoney() >= 2500 && poolCounter == 0) {
                Bank.decreaseMoney(2500);
                poolCounter += 1;
                MainActivity.setCps(MainActivity.getCps() + poolCoinIncrease);
                return true;
            }
            return false;
        }

        public static int getPoolCoinIncrease() {
            return poolCoinIncrease;
        }

        public static double getCoinIncrease() {
            return coinIncrease;
        }
    }

    public static class Computer {
        private static int computerCounter = 0;
        private static double coinIncrease = 1;
        private static double poolCoinIncrease = 2;
        private static int poolCounter = 0;
        private static double upgradePrice = 200;


        private static void setUpgradePrice(double price) {
            upgradePrice = price;
        }

        public static double getUpgradePrice() {
            return upgradePrice;
        }

        public static int getPoolCounter() {
            return poolCounter;
        }

        public static double getCoinIncrease() {
            return coinIncrease;
        }

        public static double getPoolCoinIncrease() {
            return poolCoinIncrease;
        }

        public static int getComputerCounter() {
            return computerCounter;
        }

        public static boolean buyComputer() {
            if (Bank.getMoney() >= getUpgradePrice()) {
                Bank.decreaseMoney(getUpgradePrice());
                setUpgradePrice(MainActivity.round(Math.pow(getUpgradePrice(), 1.1), 2));
                computerCounter += 1;
                MainActivity.setClickPower(MainActivity.getClickPower() + 1);
                MainActivity.setCps(MainActivity.getCps() + coinIncrease/100);
                return true;
            }
            return false;
        }

        public static boolean createPool() {
            if (computerCounter >= 10 && Bank.getMoney() >= 500 && poolCounter == 0) {
                Bank.decreaseMoney( 500);
                poolCounter += 1;
                MainActivity.setCps(MainActivity.getCps() + poolCoinIncrease);
                return true;
            }
            return false;
        }
    }

    public static class Factory {
        private static int factoryCounter = 0;
        private static double coinIncrease = 5;
        private static int poolCounter = 0;
        private static double upgradePrice = 20000;
        private static double poolCoinIncrease = 30;

        private static void setUpgradePrice(double price) {
            upgradePrice = price;
        }

        public static double getUpgradePrice() {
            return upgradePrice;
        }

        public static double getCoinIncrease() {
            return coinIncrease;
        }

        public static int getPoolCounter() {
            return poolCounter;
        }

        public static int getFactoryCounter() {
            return factoryCounter;
        }

        public static boolean buyFactory() {
            if (Bank.getMoney() >= getUpgradePrice()) {
                Bank.decreaseMoney(getUpgradePrice());
                setUpgradePrice(MainActivity.round(Math.pow(getUpgradePrice(), 1.05), 2));
                factoryCounter += 1;
                MainActivity.setCps(MainActivity.getCps() + coinIncrease);
                return true;
            }
            return false;
        }

        public static boolean createPool() {
            if (factoryCounter >= 10 && Bank.getMoney() >= 500000 && poolCounter == 0) {
                Bank.decreaseMoney(500000);
                poolCounter += 1;
                MainActivity.setCps(MainActivity.getCps() + poolCoinIncrease);
                return true;
            }
            return false;
        }
    }

    public static class Virus {
        private static double upgradePrice = 500;
        private static double counter = 0;
        private static int duration = 3600000;
        private static int interval = 1000;

        public static double getUpgradePrice() {
            return upgradePrice;
        }

        private static void setUpgradePrice(double upgradePrince) {
            upgradePrice = upgradePrince;
        }

        private static double getCounter() {
            return counter;
        }


        public static boolean buyVirus() {
            if (Bank.getMoney() >= getUpgradePrice()) {
                Bank.decreaseMoney(getUpgradePrice());
                setUpgradePrice(MainActivity.round(Math.pow(getUpgradePrice(), 1.05), 2));
                if (counter < 50) {
                    MainActivity.coinBoost(duration, interval, MainActivity.getCps() *
                            (1 / (50 - getCounter())));
                } else {
                    MainActivity.coinBoost(duration, interval, MainActivity.getCps());
                }
                return true;
            }
            return false;
        }
    }

}
