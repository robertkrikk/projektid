package teamrn.bank;

import teamrn.cryptoclickerxd.MainActivity;

public class Bank {
    private static double coins = 0;
    private static double money = 0;
    private static double coinrate = 1;

    public static void convertCoins() {
        money = MainActivity.round(money + coins * coinrate, 2);
        coins = 0;
    }


    public static void removeMoney(int i) {
        money = money - i;
    }

    public static double getMoney() {
        return money;
    }

    public static double getCoins() {
        return coins;
    }

    public static double getCoinrate() {
        return coinrate;
    }

    public static void setMoney(double moneyu) {
        money = moneyu;
    }

    public static void decreaseMoney (double moneyu) {
        money = money - moneyu;
    }

    public static void setCoinrate(double coinrateu) {
        coinrate = coinrateu;
    }

    public static void increaseCoins (double coinsu) {
        coins = coins + coinsu;
    }

    public static void setCoins(double coinsu) {
        coins = coinsu;
    }

}
