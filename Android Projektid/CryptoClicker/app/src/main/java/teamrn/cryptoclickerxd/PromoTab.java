package teamrn.cryptoclickerxd;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.Arrays;
import java.util.List;

import teamrn.promo.Promo;



public class PromoTab extends AppCompatActivity {
    public static String PACKAGE_NAME;
    public static final String ARG_SECTION_NUMBER = "section_number";
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upgrade_tab);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        PACKAGE_NAME = getApplicationContext().getPackageName();


    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        public static int arg;
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }
        public static int giveArg() {
            return arg;
        }
        public void setArg() {
            arg = getArguments().getInt(ARG_SECTION_NUMBER);

        }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            setArg();
            View rootView = inflater.inflate(R.layout.fragment_promo_tab, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(returnText(getArguments().getInt(ARG_SECTION_NUMBER)));

            ImageView picture = rootView.findViewById(R.id.upgradepic);
            picture.setImageResource(picturesReturn(getArguments().getInt(ARG_SECTION_NUMBER),
                    rootView));

            TextView name = rootView.findViewById(R.id.name);
            name.setText(upgradeName(getArguments().getInt(ARG_SECTION_NUMBER)));

            TextView upgrade = rootView.findViewById(R.id.upgrade);
            upgrade.setText(getPromoPrice(getArguments().getInt(ARG_SECTION_NUMBER)));

            Button upgradebutton = rootView.findViewById(R.id.button);
            upgradebutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    System.out.println(getArguments().getInt(ARG_SECTION_NUMBER));
                    Snackbar.make(view, buyUpgrade(getArguments().getInt(ARG_SECTION_NUMBER)),
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                }
            });

            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }
    public static String upgradeName(int i) {
        List<String> upgradetexts = Arrays.asList("HDTanel", "Twitter", "Facebook");
        return upgradetexts.get(i);
    }

    public static String getPromoPrice(int i) {
        String asd;
        switch(i) {
            case 0: asd = String.valueOf(Promo.HDTanelPromo.getPromoPrice());
                return asd;
            case 1: asd = String.valueOf(Promo.TwitterPromo.getPromoPrice());
                return asd;
            case 2: asd = String.valueOf(Promo.FacebookAdPromo.getPromoPrice());
                return asd;
            default:
                return "ASD";
        }
    }

    public static String returnText(int i) {
        List<String> upgradetexts = Arrays.asList("Tanel goes hard in gym and crypto." +
                        "The dream man for the job", "Finally there's a good use for those " +
                        "indians. Spamming the twitter!" , "Mark Zuckerberg hates" +
                        "cryptocurrency. A great place to advert one!");
        return upgradetexts.get(i);
    }

    public static String buyUpgrade(int i) {
        switch(i) {
            case 0: if (Promo.HDTanelPromo.buyPromo()) {
                return "Sorry, but HDTanel ran off with money :(((((";
            }
                return "Not enough money!";
            case 1: if (Promo.TwitterPromo.buyPromo()) {
                return "Upgrade bought! Small indian spammers now work for you.";
            }
                return "Not enough money!";
            case 2: if (Promo.FacebookAdPromo.buyPromo()) {
                return "Upgrade bought! Be ready for massive amount of scammers!";
            }
                return "Not enough money!";
            default: System.out.println("ASD");
                return "Not enough money!";
        }
    }

    public static int picturesReturn(int i, View view) {
        int id;
        switch(i) {
            case 0: id = view.getResources().getIdentifier("hd", "drawable",
                    PACKAGE_NAME);
                return id;
            case 1: id = view.getResources().getIdentifier("workers", "drawable",
                    PACKAGE_NAME);
                return id;
            case 2: id = view.getResources().getIdentifier("zucc", "drawable",
                    PACKAGE_NAME);
                return id;
            default: return 0;
        }
    }

}
