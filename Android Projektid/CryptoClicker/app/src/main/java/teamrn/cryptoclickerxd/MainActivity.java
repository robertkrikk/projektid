package teamrn.cryptoclickerxd;

import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Locale;

import teamrn.bank.Bank;
import teamrn.twitterapi.PostTweet;

public class MainActivity extends AppCompatActivity {
    ImageView mainButton;
    ImageView upgradesButton;
    ImageView bankButton;
    ImageView virusButton;
    ImageView promoButton;
    ImageView twitterButton;
    TextView coinPrice;
    TextView amount;
    TextView cpsView;
    ConstraintLayout mainLayout;
    RelativeLayout coinLayout;
    private float[] lastTouchDownXY = new float[2];
    private static int clickPower = 1;
    float xCurrentPos, yCurrentPos;
    private static double cps = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mainLayout = findViewById(R.id.mainLayout);
        coinLayout = findViewById(R.id.coinLayout);

        amount = findViewById(R.id.coins);
        coinPrice = findViewById(R.id.coinPrice);
        cpsView = findViewById(R.id.cps);

        mainButton = findViewById(R.id.coin);
        upgradesButton = findViewById(R.id.upgrades1);
        bankButton = findViewById(R.id.bankButton);
        virusButton = findViewById(R.id.virusbutton);
        promoButton = findViewById(R.id.promos);

        twitterButton = findViewById(R.id.twitterButton);

        xCurrentPos = mainButton.getLeft();
        yCurrentPos = mainButton.getTop();

        final TranslateAnimation translate = new TranslateAnimation(
                xCurrentPos, xCurrentPos, yCurrentPos, yCurrentPos+6);
        translate.setDuration(100);//speed of the animation

        View.OnTouchListener coinTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // save the X,Y coordinates
                if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    lastTouchDownXY[0] = event.getX();
                    lastTouchDownXY[1] = event.getY();
                }
                // let the touch event pass on to whoever needs it
                return false;
            }
        };

        mainButton.setOnTouchListener(coinTouchListener);
        mainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float x = lastTouchDownXY[0];
                float y = lastTouchDownXY[1];
                TextView coinTouch = createNewTextView(clickPower, x, y);

                mainButton.startAnimation(translate);
                Bank.increaseCoins(clickPower);

                coinLayout.addView(coinTouch);
                double rounded_coins = round(Bank.getCoins(), 2);
                amount.setText((Double.toString(rounded_coins)));
            }
        });
        virusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Booster.showAlert(MainActivity.this);
            }
        });
        upgradesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, UpgradeTab.class);
                MainActivity.this.startActivity(myIntent);
            }
        });

        bankButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, BankScreen.class);
                MainActivity.this.startActivity(myIntent);
            }
        });
        promoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, PromoTab.class);
                MainActivity.this.startActivity(myIntent);
            }
        });
        twitterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try  {
                            PostTweet.postToTwitter();
                            twitterButton.setImageResource(0);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();
            }
        });
        coinIncrease();
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void coinIncrease() {
        final int time = 100;
        Thread increaseCoins = new Thread() {
            @Override
            public void run() {
                while(!isInterrupted()) {
                    try {
                        Thread.sleep(time);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Bank.increaseCoins(cps / 10);
                                double rounded_coins = round(Bank.getCoins(), 2);
                                double rounded_cps = round(cps, 2);
                                double rounded_price = round(Bank.getCoinrate(), 2);
                                amount.setText((Double.toString(rounded_coins)));
                                cpsView.setText(String.format(Locale.ENGLISH, "CpS: %f", rounded_cps));
                                coinPrice.setText(String.format(Locale.ENGLISH, "Coin price: %f ", rounded_price));
                            }
                        });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        increaseCoins.start();
    }

    public static void coinBoost(int duration, int interval, final double increase) {
        new CountDownTimer(duration, interval) {
            public void onTick(long millisUntilFinished) {
                Bank.increaseCoins(increase);
            }
            public void onFinish() {
            }
        }.start();
    }

    private TextView createNewTextView(int amount, float x, float y) {
        final TextView textView = new TextView(MainActivity.this);
        textView.setText(String.format(Locale.ENGLISH, "+%d", amount));
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(48);
        textView.setX(x - 40);
        textView.setY(y - 40);

        AnimationSet set = new AnimationSet(false);
        Animation trAnimation = new TranslateAnimation(0, 0, 0, 30);
        trAnimation.setDuration(1000);

        set.addAnimation(trAnimation);
        Animation anim = new AlphaAnimation(1.0f, 0.0f);
        anim.setDuration(1000);
        set.addAnimation(anim);

        Animation.AnimationListener destroy = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                textView.setVisibility(View.GONE);
                coinLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        coinLayout.removeView(textView);
                    }
                }, 500);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };
        set.setAnimationListener(destroy);

        textView.startAnimation(set);

        return textView;
    }

    public static void setCps(double cps) {
        MainActivity.cps = cps;
    }

    public static void setClickPower(int clickPower) {
        MainActivity.clickPower = clickPower;
    }

    public static double getCps() {
        return cps;
    }

    public static int getClickPower() {
        return clickPower;
    }
}
