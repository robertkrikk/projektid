package teamrn.cryptoclickerxd;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

import teamrn.bank.Bank;

public class BankScreen extends AppCompatActivity {
    ImageView moneyConvert;
    TextView moneyAmount;
    TextView rate;
    TextView ratetext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_screen);

        moneyConvert = findViewById(R.id.moneyConvert);

        moneyAmount = findViewById(R.id.money);
        moneyAmount.setText(String.valueOf(Bank.getMoney() + "$"));
        moneyAmount.setTextSize(50);

        ratetext = findViewById(R.id.ratetext);
        ratetext.setText(R.string.rateText);

        rate = findViewById(R.id.currentrate);
        rate.setText(String.valueOf(Bank.getCoinrate() + "$"));

        moneyConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bank.convertCoins();
                Long asd = Double.valueOf(Bank.getMoney()).longValue();
                moneyAmount.setText(String.format(Locale.ENGLISH, "%d$", asd));
                //String.valueOf(Bank.getMoney() + "$"));
            }
        });


    }
}
