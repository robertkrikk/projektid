package teamrn.cryptoclickerxd;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import teamrn.upgrades.Upgrades;



public class UpgradeTab extends AppCompatActivity {
    public static String PACKAGE_NAME;
    public static final String ARG_SECTION_NUMBER = "section_number";
    public static Intent intent;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upgrade_tab);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        intent = getIntent();

        PACKAGE_NAME = getApplicationContext().getPackageName();


    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        public static int arg;
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }
        public static int giveArg() {
            return arg;
        }
        public void setArg() {
            arg = getArguments().getInt(ARG_SECTION_NUMBER);

        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            setArg();
            View rootView = inflater.inflate(R.layout.fragment_upgrade_tab, container, false);
            TextView textView = rootView.findViewById(R.id.section_label);
            textView.setText(returnText(getArguments().getInt(ARG_SECTION_NUMBER)));

            ImageView picture = rootView.findViewById(R.id.upgradepic);
            picture.setImageResource(picturesReturn(getArguments().getInt(ARG_SECTION_NUMBER),
                    rootView));

            TextView upgradeprice = rootView.findViewById(R.id.upgrade);
            upgradeprice.setText(getUpgradePrice(getArguments().getInt(ARG_SECTION_NUMBER)) + "$");

            TextView cps = rootView.findViewById(R.id.cps);
            cps.setText(String.format(Locale.ENGLISH, "CpS: %s",
                    getCps(getArguments().getInt(ARG_SECTION_NUMBER))));

            TextView units = rootView.findViewById(R.id.units);
            units.setText(String.format(Locale.ENGLISH, "Units: %s",
                    getUnits(getArguments().getInt(ARG_SECTION_NUMBER))));

            TextView name = rootView.findViewById(R.id.name);
            name.setText(upgradeName(getArguments().getInt(ARG_SECTION_NUMBER)));

            Button upgradebutton = rootView.findViewById(R.id.button);
            upgradebutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, buyUpgrade(getArguments().getInt(ARG_SECTION_NUMBER)),
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();


                }
            });

            Button poolButton = rootView.findViewById(R.id.poolButton);
            poolButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, buyPool(getArguments().getInt(ARG_SECTION_NUMBER)),
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });

            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position );
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }
    }
    public static String upgradeName(int i) {
        List<String> upgradetexts = Arrays.asList("Computer", "GPU", "ASIC",
                "Factory");
        return upgradetexts.get(i);
    }
    public static String returnText(int i) {
        List<String> upgradetexts = Arrays.asList("Your granny's computer. Lightning" +
                "fast shit", "Yet another overpriced GPU! Why you mine so much?", "Some epic mining" +
                        " thing. Dunno what it is actually.", "What you get if you add factory and " +
                "mining together? Mining factory. Amazing?");
        return upgradetexts.get(i);
    }
    public static String getUpgradePrice(int i) {
        String asd;
        switch(i) {
            case 0: asd = String.valueOf(Upgrades.Computer.getUpgradePrice());
            return asd;
            case 1: asd = String.valueOf(Upgrades.GPU.getUpgradePrice());
            return asd;
            case 2: asd = String.valueOf(Upgrades.ASIC.getUpgradePrice());
            return asd;
            case 3: asd = String.valueOf(Upgrades.Factory.getUpgradePrice());
                return asd;
            default:
                return "ASD";
        }
    }
    public static String getCps(int i) {
        String asd;
        switch(i) {
            case 0: asd = String.valueOf(Upgrades.Computer.getCoinIncrease());
                return asd;
            case 1: asd = String.valueOf(Upgrades.GPU.getCoinIncrease());
                return asd;
            case 2: asd = String.valueOf(Upgrades.ASIC.getCoinIncrease());
                return asd;
            case 3: asd = String.valueOf(Upgrades.Factory.getCoinIncrease());
                return asd;
            default:
                return "ASD";
        }
    }
    public static String getUnits(int i) {
        String asd;
        switch(i) {
            case 0: asd = String.valueOf(Upgrades.Computer.getComputerCounter());
                return asd;
            case 1: asd = String.valueOf(Upgrades.GPU.getGpuCounter());
                return asd;
            case 2: asd = String.valueOf(Upgrades.ASIC.getAsicCounter());
                return asd;
            case 3: asd = String.valueOf(Upgrades.Factory.getFactoryCounter());
                return asd;
            default:
                return "ASD";
        }
    }
    public static int picturesReturn(int i, View view) {
        int id;
        switch(i) {
            case 0: id = view.getResources().getIdentifier("pc", "drawable",
                    PACKAGE_NAME);
                    return id;
            case 1: id = view.getResources().getIdentifier("gpu", "drawable",
                    PACKAGE_NAME);
                return id;
            case 2: id = view.getResources().getIdentifier("asic", "drawable",
                    PACKAGE_NAME);
                return id;
            case 3: id = view.getResources().getIdentifier("factory", "drawable",
                    PACKAGE_NAME);
                return id;
            default: return 0;
        }
    }
    public static String buyUpgrade(int i) {
        switch(i) {
            case 0: if (Upgrades.Computer.buyComputer()) {
                    return "Upgrade bought!";
                }
                return "Not enough money!";
            case 1: if (Upgrades.GPU.buyGPU()) {
                    return "Upgrade bought!";
                }
                return "Not enough money!";
            case 2: if (Upgrades.ASIC.buyASIC()) {
                    return "Upgrade bought!";
                }
                return "Not enough money!";
            case 3: if (Upgrades.Factory.buyFactory()) {
                    return "Upgrade bought!";
                }
                return "Not enough money!";
            case 4: if (Upgrades.Virus.buyVirus()) {
                    return "Upgrade bought!";
                }
                return "Not enough money!";
            default: System.out.println("ASD");
                return "Not enough money!";
        }
    }

    public static String buyPool(int i) {
        switch(i) {
            case 0: if (Upgrades.Computer.createPool()) {
                return "Upgrade bought!";
            } else {
                if (Upgrades.Computer.getComputerCounter() < 10) {
                    return "Not enough computers!";
                } else if (Upgrades.Computer.getPoolCounter() == 1) {
                    return "You can only have 1 pool for computers!";
                }
                return "Not enough money!";
            }
            case 1: if (Upgrades.GPU.createPool()) {
                return "Upgrade bought!";
            } else {
                if (Upgrades.GPU.getGpuCounter() < 10) {
                    return "Not enough GPUs!";
                } else if (Upgrades.GPU.getPoolCounter() == 1) {
                    return "You can only have 1 pool for GPUs!";
                }
                return "Not enough money!";
            }
            case 2: if (Upgrades.ASIC.createPool()) {
                return "Upgrade bought!";
            } else {
                if (Upgrades.ASIC.getAsicCounter() < 10) {
                    return "Not enough ASIC-s!";
                } else if (Upgrades.ASIC.getPoolCounter() == 1) {
                    return "You can only have 1 pool for ASIC-s!";
                }
                return "Not enough money!";
            }
            case 3: if (Upgrades.Factory.createPool()) {
                return "Upgrade bought!";
            } else {
                if (Upgrades.Factory.getFactoryCounter() < 10) {
                    return "Not enough Factories!";
                } else if (Upgrades.Factory.getPoolCounter() == 1) {
                    return "You can only have 1 pool for factories!";
                }
                return "Not enough money!";
            }
            default: System.out.println("ASD");
                return "Not enough money!";
        }
    }

}
